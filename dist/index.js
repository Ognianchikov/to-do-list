/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/app-content/app-content.css":
/*!***********************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/app-content/app-content.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".appContent {\\r\\n    top: 3.92rem;\\r\\n    position: relative;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/app-content/app-content.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main-content/main-content.css":
/*!******************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main-content/main-content.css ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".mainContent {\\r\\n    overflow-y: auto;\\r\\n    height: 27rem;\\r\\n    \\r\\n}\\r\\n\\r\\n#modalContent {\\r\\n    border: 0.1rem;\\r\\n    border-color: rgb(255, 255, 255);\\r\\n    border-style: solid;\\r\\n}\\r\\n\\r\\n#modalHeaderText {\\r\\n    color: rgb(255, 255, 255);\\r\\n    font-size: 3rem;\\r\\n}\\r\\n\\r\\n#modalBodyForm {\\r\\n    font-size: 1.35em;\\r\\n}\\r\\n.ui-widget-content {\\r\\n    background: linear-gradient(#fa2bb5, #9198e5);\\r\\n    color: white;\\r\\n    border: 0;\\r\\n}\\r\\n\\r\\n.ui-widget-header {\\r\\n    border: 0;\\r\\n    background: 0;\\r\\n    color: white;\\r\\n    font-size: 1.5rem;\\r\\n    border: 0;\\r\\n}\\r\\n\\r\\n.ui-state-default {\\r\\n    font-size: 1.2em;\\r\\n}\\r\\n\\r\\n.ui-datepicker-hover {\\r\\n    background: 0;\\r\\n}\\r\\n\\r\\n.ui-datepicker-header {\\r\\n    height: 3.35em;\\r\\n    padding: 0.65em;\\r\\n    border: 0;\\r\\n}\\r\\n\\r\\n.ui-button:not([type=\\\"reset\\\"]){\\r\\n    background: #007bff;\\r\\n    color: white;\\r\\n    border: 2;\\r\\n}\\r\\n.ui-button:not([type=\\\"reset\\\"]):hover {\\r\\n    background: #fdfeff;\\r\\n    color:  #007bff;\\r\\n    border: 2;\\r\\n}\\r\\n\\r\\n.tasks > tr { \\r\\n    font-size: 1.5rem;\\r\\n    color: rgb(0, 81, 255);\\r\\n}\\r\\n\\r\\n\\r\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/app-content/main/main-content/main-content.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main-date/main-date.css":
/*!************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main-date/main-date.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"#mainDateContent {\\r\\n    padding-left: 2rem;\\r\\n    padding-top: 5rem;   \\r\\n}\\r\\n\\r\\n#activeNavbarItem {\\r\\n    color: white;\\r\\n    font-weight: normal;\\r\\n}\\r\\n\\r\\n#currentDateItem {\\r\\n    color: white;\\r\\n    font-weight: lighter;\\r\\n    font-size: 1.5rem;\\r\\n}\\r\\n\\r\\n.mainDate {\\r\\n    display: inline-block;\\r\\n    width: 100%;\\r\\n    height: 14rem;\\r\\n    background-repeat: no-repeat center center;\\r\\n    background-size: 100% 100%;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/app-content/main/main-date/main-date.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main.css":
/*!*********************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".main {\\r\\n    background-color: rgb(248, 248, 248);\\r\\n    height: 100vh;\\r\\n    padding: 0;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/app-content/main/main.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/app-content/navbar/navbar.css":
/*!*************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/app-content/navbar/navbar.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".navbar {\\r\\n    color: rgb(255, 255, 255);\\r\\n}\\r\\n\\r\\n.navbar > button {\\r\\n    width: 100%;\\r\\n}\\r\\n\\r\\n.plannedTasks {\\r\\n    width: 100%;\\r\\n}\\r\\n\\r\\n.plannedTasksBtn {\\r\\n    width: 100%;\\r\\n    margin-left: 0;\\r\\n}\\r\\n\\r\\n.dropdown-item {\\r\\n    font-size: 1.5rem;\\r\\n    font-weight: normal;\\r\\n}\\r\\n\\r\\n.dropdown-menu.show {\\r\\n    left: 15rem;\\r\\n}\\r\\n\\r\\n.onAWeek {\\r\\n    color: rgb(42, 109, 255);\\r\\n    font-weight: bold;\\r\\n}\\r\\n\\r\\nh3 {\\r\\n    font-size: 2rem;\\r\\n}\\r\\n\\r\\na {\\r\\n    font-size: 2rem;\\r\\n}\\r\\n\\r\\n.getModalWindowItem {\\r\\n    width: 100%;\\r\\n    padding-left: 2rem;\\r\\n}\\r\\n\\r\\n#modalBtn {\\r\\n    border-radius: 50%;\\r\\n    margin: 1rem 1rem 1rem 1rem;\\r\\n}\\r\\n\\r\\n#modalBtnImg {\\r\\n    width: 1rem;\\r\\n    height: 1.5rem;\\r\\n}\\r\\n\\r\\n.modalText {\\r\\n    font-size: 2.0rem;\\r\\n    color: rgb(42, 109, 255);\\r\\n    font-weight: normal;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/app-content/navbar/navbar.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/app.css":
/*!***************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/app.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"html {\\r\\n    overflow: hidden;\\r\\n}\\r\\n\\r\\n.modal-open {\\r\\n    padding-right: 0;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/app.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/add-modal-window/modal-window.css":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/add-modal-window/modal-window.css ***!
  \****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".formBtnGroup {\\r\\n    padding-left: 17rem;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/add-modal-window/modal-window.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/edit-modal-window/modal-window.css":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/edit-modal-window/modal-window.css ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".editFormBtnGroup {\\r\\n    padding-left: 17rem;\\r\\n}\\r\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/edit-modal-window/modal-window.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.css":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.css ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".newTask {\\r\\n    width: 100%;\\r\\n    background-color: rgb(255, 255, 255);\\r\\n}  \\r\\n\\r\\n.toDo-btn-label {\\r\\n    height: 2.5rem;\\r\\n    width: 2.5rem;\\r\\n    border-radius: 50%;\\r\\n    border-width: 2px;\\r\\n}\\r\\n\\r\\n.taskTitle {\\r\\n    font-weight: normal;\\r\\n    font-size: 1.8rem;\\r\\n    color: rgb(77, 77, 77);\\r\\n}\\r\\n\\r\\n.importanceTask {\\r\\n    height: 2rem;\\r\\n}\\r\\n\\r\\n.importanceTaskBtn {\\r\\n    font-size: 1rem;\\r\\n}\\r\\n\\r\\n.dateDeadine {\\r\\n    font-size: 1rem;\\r\\n}\\r\\n\\r\\n.toDoBtn {\\r\\n    height: 2.5rem;\\r\\n}\\r\\n\\r\\n.edit-task-btn {\\r\\n    background-color: rgb(255, 255, 255);\\r\\n    border: 0;\\r\\n}\\r\\n\\r\\n.deleteTaskBtn {\\r\\n    background-color: rgb(255, 255, 255);\\r\\n    border: 0;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/header.css":
/*!*************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/header.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".header {\\r\\n    background: rgb(84, 54, 255);\\r\\n    position: fixed;\\r\\n    z-index: 1;\\r\\n    top: 0px;\\r\\n    left: 0px;\\r\\n    width: 100%;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/header.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\n// eslint-disable-next-line func-names\n\nmodule.exports = function (useSourceMap) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = cssWithMappingToString(item, useSourceMap);\n\n      if (item[2]) {\n        return \"@media \".concat(item[2], \" {\").concat(content, \"}\");\n      }\n\n      return content;\n    }).join('');\n  }; // import a list of modules into the list\n  // eslint-disable-next-line func-names\n\n\n  list.i = function (modules, mediaQuery, dedupe) {\n    if (typeof modules === 'string') {\n      // eslint-disable-next-line no-param-reassign\n      modules = [[null, modules, '']];\n    }\n\n    var alreadyImportedModules = {};\n\n    if (dedupe) {\n      for (var i = 0; i < this.length; i++) {\n        // eslint-disable-next-line prefer-destructuring\n        var id = this[i][0];\n\n        if (id != null) {\n          alreadyImportedModules[id] = true;\n        }\n      }\n    }\n\n    for (var _i = 0; _i < modules.length; _i++) {\n      var item = [].concat(modules[_i]);\n\n      if (dedupe && alreadyImportedModules[item[0]]) {\n        // eslint-disable-next-line no-continue\n        continue;\n      }\n\n      if (mediaQuery) {\n        if (!item[2]) {\n          item[2] = mediaQuery;\n        } else {\n          item[2] = \"\".concat(mediaQuery, \" and \").concat(item[2]);\n        }\n      }\n\n      list.push(item);\n    }\n  };\n\n  return list;\n};\n\nfunction cssWithMappingToString(item, useSourceMap) {\n  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring\n\n  var cssMapping = item[3];\n\n  if (!cssMapping) {\n    return content;\n  }\n\n  if (useSourceMap && typeof btoa === 'function') {\n    var sourceMapping = toComment(cssMapping);\n    var sourceURLs = cssMapping.sources.map(function (source) {\n      return \"/*# sourceURL=\".concat(cssMapping.sourceRoot || '').concat(source, \" */\");\n    });\n    return [content].concat(sourceURLs).concat([sourceMapping]).join('\\n');\n  }\n\n  return [content].join('\\n');\n} // Adapted from convert-source-map (MIT)\n\n\nfunction toComment(sourceMap) {\n  // eslint-disable-next-line no-undef\n  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));\n  var data = \"sourceMappingURL=data:application/json;charset=utf-8;base64,\".concat(base64);\n  return \"/*# \".concat(data, \" */\");\n}\n\n//# sourceURL=webpack:///./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar isOldIE = function isOldIE() {\n  var memo;\n  return function memorize() {\n    if (typeof memo === 'undefined') {\n      // Test for IE <= 9 as proposed by Browserhacks\n      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n      // Tests for existence of standard globals is to allow style-loader\n      // to operate correctly into non-standard environments\n      // @see https://github.com/webpack-contrib/style-loader/issues/177\n      memo = Boolean(window && document && document.all && !window.atob);\n    }\n\n    return memo;\n  };\n}();\n\nvar getTarget = function getTarget() {\n  var memo = {};\n  return function memorize(target) {\n    if (typeof memo[target] === 'undefined') {\n      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself\n\n      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n        try {\n          // This will throw an exception if access to iframe is blocked\n          // due to cross-origin restrictions\n          styleTarget = styleTarget.contentDocument.head;\n        } catch (e) {\n          // istanbul ignore next\n          styleTarget = null;\n        }\n      }\n\n      memo[target] = styleTarget;\n    }\n\n    return memo[target];\n  };\n}();\n\nvar stylesInDom = [];\n\nfunction getIndexByIdentifier(identifier) {\n  var result = -1;\n\n  for (var i = 0; i < stylesInDom.length; i++) {\n    if (stylesInDom[i].identifier === identifier) {\n      result = i;\n      break;\n    }\n  }\n\n  return result;\n}\n\nfunction modulesToDom(list, options) {\n  var idCountMap = {};\n  var identifiers = [];\n\n  for (var i = 0; i < list.length; i++) {\n    var item = list[i];\n    var id = options.base ? item[0] + options.base : item[0];\n    var count = idCountMap[id] || 0;\n    var identifier = \"\".concat(id, \" \").concat(count);\n    idCountMap[id] = count + 1;\n    var index = getIndexByIdentifier(identifier);\n    var obj = {\n      css: item[1],\n      media: item[2],\n      sourceMap: item[3]\n    };\n\n    if (index !== -1) {\n      stylesInDom[index].references++;\n      stylesInDom[index].updater(obj);\n    } else {\n      stylesInDom.push({\n        identifier: identifier,\n        updater: addStyle(obj, options),\n        references: 1\n      });\n    }\n\n    identifiers.push(identifier);\n  }\n\n  return identifiers;\n}\n\nfunction insertStyleElement(options) {\n  var style = document.createElement('style');\n  var attributes = options.attributes || {};\n\n  if (typeof attributes.nonce === 'undefined') {\n    var nonce =  true ? __webpack_require__.nc : undefined;\n\n    if (nonce) {\n      attributes.nonce = nonce;\n    }\n  }\n\n  Object.keys(attributes).forEach(function (key) {\n    style.setAttribute(key, attributes[key]);\n  });\n\n  if (typeof options.insert === 'function') {\n    options.insert(style);\n  } else {\n    var target = getTarget(options.insert || 'head');\n\n    if (!target) {\n      throw new Error(\"Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.\");\n    }\n\n    target.appendChild(style);\n  }\n\n  return style;\n}\n\nfunction removeStyleElement(style) {\n  // istanbul ignore if\n  if (style.parentNode === null) {\n    return false;\n  }\n\n  style.parentNode.removeChild(style);\n}\n/* istanbul ignore next  */\n\n\nvar replaceText = function replaceText() {\n  var textStore = [];\n  return function replace(index, replacement) {\n    textStore[index] = replacement;\n    return textStore.filter(Boolean).join('\\n');\n  };\n}();\n\nfunction applyToSingletonTag(style, index, remove, obj) {\n  var css = remove ? '' : obj.media ? \"@media \".concat(obj.media, \" {\").concat(obj.css, \"}\") : obj.css; // For old IE\n\n  /* istanbul ignore if  */\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = replaceText(index, css);\n  } else {\n    var cssNode = document.createTextNode(css);\n    var childNodes = style.childNodes;\n\n    if (childNodes[index]) {\n      style.removeChild(childNodes[index]);\n    }\n\n    if (childNodes.length) {\n      style.insertBefore(cssNode, childNodes[index]);\n    } else {\n      style.appendChild(cssNode);\n    }\n  }\n}\n\nfunction applyToTag(style, options, obj) {\n  var css = obj.css;\n  var media = obj.media;\n  var sourceMap = obj.sourceMap;\n\n  if (media) {\n    style.setAttribute('media', media);\n  } else {\n    style.removeAttribute('media');\n  }\n\n  if (sourceMap && btoa) {\n    css += \"\\n/*# sourceMappingURL=data:application/json;base64,\".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), \" */\");\n  } // For old IE\n\n  /* istanbul ignore if  */\n\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = css;\n  } else {\n    while (style.firstChild) {\n      style.removeChild(style.firstChild);\n    }\n\n    style.appendChild(document.createTextNode(css));\n  }\n}\n\nvar singleton = null;\nvar singletonCounter = 0;\n\nfunction addStyle(obj, options) {\n  var style;\n  var update;\n  var remove;\n\n  if (options.singleton) {\n    var styleIndex = singletonCounter++;\n    style = singleton || (singleton = insertStyleElement(options));\n    update = applyToSingletonTag.bind(null, style, styleIndex, false);\n    remove = applyToSingletonTag.bind(null, style, styleIndex, true);\n  } else {\n    style = insertStyleElement(options);\n    update = applyToTag.bind(null, style, options);\n\n    remove = function remove() {\n      removeStyleElement(style);\n    };\n  }\n\n  update(obj);\n  return function updateStyle(newObj) {\n    if (newObj) {\n      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {\n        return;\n      }\n\n      update(obj = newObj);\n    } else {\n      remove();\n    }\n  };\n}\n\nmodule.exports = function (list, options) {\n  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n  // tags it will allow on a page\n\n  if (!options.singleton && typeof options.singleton !== 'boolean') {\n    options.singleton = isOldIE();\n  }\n\n  list = list || [];\n  var lastIdentifiers = modulesToDom(list, options);\n  return function update(newList) {\n    newList = newList || [];\n\n    if (Object.prototype.toString.call(newList) !== '[object Array]') {\n      return;\n    }\n\n    for (var i = 0; i < lastIdentifiers.length; i++) {\n      var identifier = lastIdentifiers[i];\n      var index = getIndexByIdentifier(identifier);\n      stylesInDom[index].references--;\n    }\n\n    var newLastIdentifiers = modulesToDom(newList, options);\n\n    for (var _i = 0; _i < lastIdentifiers.length; _i++) {\n      var _identifier = lastIdentifiers[_i];\n\n      var _index = getIndexByIdentifier(_identifier);\n\n      if (stylesInDom[_index].references === 0) {\n        stylesInDom[_index].updater();\n\n        stylesInDom.splice(_index, 1);\n      }\n    }\n\n    lastIdentifiers = newLastIdentifiers;\n  };\n};\n\n//# sourceURL=webpack:///./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js?");

/***/ }),

/***/ "./src/app/app-content/app-content.css":
/*!*********************************************!*\
  !*** ./src/app/app-content/app-content.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./app-content.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/app-content/app-content.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/app-content/app-content.css?");

/***/ }),

/***/ "./src/app/app-content/app-content.js":
/*!********************************************!*\
  !*** ./src/app/app-content/app-content.js ***!
  \********************************************/
/*! exports provided: AppContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AppContent\", function() { return AppContent; });\n/* harmony import */ var _app_content_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app-content.css */ \"./src/app/app-content/app-content.css\");\n/* harmony import */ var _app_content_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_app_content_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _navbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navbar */ \"./src/app/app-content/navbar/index.js\");\n/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main */ \"./src/app/app-content/main/index.js\");\n\n\n\nfunction AppContent() {\n  var navbar = Object(_navbar__WEBPACK_IMPORTED_MODULE_1__[\"NavBar\"])();\n  var main = Object(_main__WEBPACK_IMPORTED_MODULE_2__[\"Main\"])();\n  var appContent = document.createElement('div');\n  appContent.classList.add('row', 'align-items-start', 'app-content', 'display-3', 'appContent');\n  appContent.append(navbar, main);\n  return appContent;\n}\n\n//# sourceURL=webpack:///./src/app/app-content/app-content.js?");

/***/ }),

/***/ "./src/app/app-content/index.js":
/*!**************************************!*\
  !*** ./src/app/app-content/index.js ***!
  \**************************************/
/*! exports provided: AppContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _app_content__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app-content */ \"./src/app/app-content/app-content.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"AppContent\", function() { return _app_content__WEBPACK_IMPORTED_MODULE_0__[\"AppContent\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/app-content/index.js?");

/***/ }),

/***/ "./src/app/app-content/main/index.js":
/*!*******************************************!*\
  !*** ./src/app/app-content/main/index.js ***!
  \*******************************************/
/*! exports provided: Main */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main */ \"./src/app/app-content/main/main.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Main\", function() { return _main__WEBPACK_IMPORTED_MODULE_0__[\"Main\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/app-content/main/index.js?");

/***/ }),

/***/ "./src/app/app-content/main/main-content/index.js":
/*!********************************************************!*\
  !*** ./src/app/app-content/main/main-content/index.js ***!
  \********************************************************/
/*! exports provided: MainContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _main_content__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main-content */ \"./src/app/app-content/main/main-content/main-content.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"MainContent\", function() { return _main_content__WEBPACK_IMPORTED_MODULE_0__[\"MainContent\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/app-content/main/main-content/index.js?");

/***/ }),

/***/ "./src/app/app-content/main/main-content/main-content.css":
/*!****************************************************************!*\
  !*** ./src/app/app-content/main/main-content/main-content.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./main-content.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main-content/main-content.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/app-content/main/main-content/main-content.css?");

/***/ }),

/***/ "./src/app/app-content/main/main-content/main-content.js":
/*!***************************************************************!*\
  !*** ./src/app/app-content/main/main-content/main-content.js ***!
  \***************************************************************/
/*! exports provided: MainContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MainContent\", function() { return MainContent; });\n/* harmony import */ var _main_content_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main-content.css */ \"./src/app/app-content/main/main-content/main-content.css\");\n/* harmony import */ var _main_content_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_main_content_css__WEBPACK_IMPORTED_MODULE_0__);\n\nfunction MainContent() {\n  var mainContent = document.createElement('div');\n  mainContent.id = 'mainContent';\n  mainContent.classList.add('flex-column', 'mainContent', 'tasks');\n  return mainContent;\n}\n\n//# sourceURL=webpack:///./src/app/app-content/main/main-content/main-content.js?");

/***/ }),

/***/ "./src/app/app-content/main/main-date/index.js":
/*!*****************************************************!*\
  !*** ./src/app/app-content/main/main-date/index.js ***!
  \*****************************************************/
/*! exports provided: MainDate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _main_date__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main-date */ \"./src/app/app-content/main/main-date/main-date.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"MainDate\", function() { return _main_date__WEBPACK_IMPORTED_MODULE_0__[\"MainDate\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/app-content/main/main-date/index.js?");

/***/ }),

/***/ "./src/app/app-content/main/main-date/main-date.css":
/*!**********************************************************!*\
  !*** ./src/app/app-content/main/main-date/main-date.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./main-date.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main-date/main-date.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/app-content/main/main-date/main-date.css?");

/***/ }),

/***/ "./src/app/app-content/main/main-date/main-date.js":
/*!*********************************************************!*\
  !*** ./src/app/app-content/main/main-date/main-date.js ***!
  \*********************************************************/
/*! exports provided: MainDate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MainDate\", function() { return MainDate; });\n/* harmony import */ var _main_date_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main-date.css */ \"./src/app/app-content/main/main-date/main-date.css\");\n/* harmony import */ var _main_date_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_main_date_css__WEBPACK_IMPORTED_MODULE_0__);\n\nfunction MainDate() {\n  var mainDate = document.createElement('div');\n  var mainDateContent = document.createElement('div');\n  var activeNavbarItem = document.createElement('div');\n  var currentDateItem = document.createElement('div');\n  mainDate.append(mainDateContent);\n  mainDateContent.append(activeNavbarItem);\n  mainDateContent.append(currentDateItem);\n  activeNavbarItem.classList.add('display-3');\n  mainDate.id = 'mainDate';\n  mainDateContent.id = 'mainDateContent';\n  activeNavbarItem.id = 'activeNavbarItem';\n  currentDateItem.id = 'currentDateItem';\n  mainDate.classList.add('mainDate');\n  mainDate.style.background = 'url(../src/app/img/tasksBg.jpg';\n  mainDate.style.backgroundRepeat = 'no-repeat center center';\n  mainDate.style.backgroundSize = '100% 100%';\n  mainDateContent.classList.add('flex-column', 'mainDateContent');\n  activeNavbarItem.innerHTML = 'Tasks';\n  return mainDate;\n}\n\n//# sourceURL=webpack:///./src/app/app-content/main/main-date/main-date.js?");

/***/ }),

/***/ "./src/app/app-content/main/main.css":
/*!*******************************************!*\
  !*** ./src/app/app-content/main/main.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./main.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/app-content/main/main.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/app-content/main/main.css?");

/***/ }),

/***/ "./src/app/app-content/main/main.js":
/*!******************************************!*\
  !*** ./src/app/app-content/main/main.js ***!
  \******************************************/
/*! exports provided: Main */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Main\", function() { return Main; });\n/* harmony import */ var _main_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main.css */ \"./src/app/app-content/main/main.css\");\n/* harmony import */ var _main_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_main_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _main_date__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./main-date */ \"./src/app/app-content/main/main-date/index.js\");\n/* harmony import */ var _main_content__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main-content */ \"./src/app/app-content/main/main-content/index.js\");\n\n\n\nfunction Main() {\n  var mainDate = Object(_main_date__WEBPACK_IMPORTED_MODULE_1__[\"MainDate\"])();\n  var mainContent = Object(_main_content__WEBPACK_IMPORTED_MODULE_2__[\"MainContent\"])();\n  var main = document.createElement('main');\n  main.id = 'main';\n  main.classList.add('col-9', 'main');\n  main.append(mainDate, mainContent);\n  return main;\n}\n\n//# sourceURL=webpack:///./src/app/app-content/main/main.js?");

/***/ }),

/***/ "./src/app/app-content/navbar/index.js":
/*!*********************************************!*\
  !*** ./src/app/app-content/navbar/index.js ***!
  \*********************************************/
/*! exports provided: NavBar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _navbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navbar */ \"./src/app/app-content/navbar/navbar.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"NavBar\", function() { return _navbar__WEBPACK_IMPORTED_MODULE_0__[\"NavBar\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/app-content/navbar/index.js?");

/***/ }),

/***/ "./src/app/app-content/navbar/navbar.css":
/*!***********************************************!*\
  !*** ./src/app/app-content/navbar/navbar.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./navbar.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/app-content/navbar/navbar.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/app-content/navbar/navbar.css?");

/***/ }),

/***/ "./src/app/app-content/navbar/navbar.js":
/*!**********************************************!*\
  !*** ./src/app/app-content/navbar/navbar.js ***!
  \**********************************************/
/*! exports provided: NavBar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"NavBar\", function() { return NavBar; });\n/* harmony import */ var _navbar_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navbar.css */ \"./src/app/app-content/navbar/navbar.css\");\n/* harmony import */ var _navbar_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_navbar_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _planned_tasks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./planned-tasks */ \"./src/app/app-content/navbar/planned-tasks.js\");\n\n\nfunction NavBar() {\n  var navbar = document.createElement('nav');\n  var myDay = document.createElement('button');\n  var myDayLogo = document.createElement('img');\n  var myDayName = document.createElement('a');\n  var tasks = document.createElement('button');\n  var tasksLogo = document.createElement('img');\n  var tasksName = document.createElement('a');\n  var importantTasks = document.createElement('button');\n  var importantTasksLogo = document.createElement('img');\n  var importantTasksName = document.createElement('a');\n  var plannedTasks = Object(_planned_tasks__WEBPACK_IMPORTED_MODULE_1__[\"PlannedTasks\"])();\n  var getModalWindowItem = document.createElement('div');\n  var modalBtn = document.createElement('button');\n  var modalBtnImg = document.createElement('img');\n  var modalText = document.createElement('div');\n  navbar.id = 'navbar';\n  navbar.classList.add('col-3', 'flex-column', 'p-0', 'navbar');\n  myDay.id = 'myDay';\n  myDayLogo.id = 'myDayLogo';\n  myDayName.id = 'myDayName';\n  myDay.classList.add('btn', 'd-flex', 'row', 'justify-content-start', 'align-items-center');\n  myDayLogo.src = '../src/app/img/myDayLogo.png';\n  myDayLogo.style.width = '2rem';\n  myDayLogo.style.marginLeft = '2.5rem';\n  myDayName.innerHTML = 'My day';\n  myDayName.classList.add('pl-3', 'nav-link');\n  tasks.id = 'tasks';\n  tasksLogo.id = 'tasksLogo';\n  tasksName.id = 'tasksName';\n  tasks.classList.add('btn', 'd-flex', 'row', 'justify-content-start', 'align-items-center');\n  tasksName.classList.add('pl-3', 'nav-link');\n  tasksLogo.src = '../src/app/img/tasksLogo.png';\n  tasksLogo.style.width = '2rem';\n  tasksLogo.style.marginLeft = '2.5rem';\n  tasksName.innerHTML = 'Tasks';\n  importantTasks.id = 'importantTasks';\n  importantTasksLogo.id = 'importantTasksLogo';\n  importantTasksName.id = 'importantTasksName';\n  importantTasks.classList.add('btn', 'd-flex', 'row', 'justify-content-start', 'align-items-center', 'importantTasks');\n  importantTasksName.classList.add('pl-3', 'nav-link');\n  importantTasksLogo.src = '../src/app/img/importantTasksLogo.png';\n  importantTasksLogo.style.width = '2rem';\n  importantTasksLogo.style.marginLeft = '2.5rem';\n  importantTasksName.innerHTML = 'Important';\n  getModalWindowItem.id = 'getModalWindowItem';\n  modalBtn.id = 'modalBtn';\n  modalBtnImg.id = 'modalBtnImg';\n  modalText.id = 'mainModalText';\n  getModalWindowItem.classList.add('row', 'justify-content-start', 'align-items-center', 'getModalWindowItem');\n  modalBtn.classList.add('btn', 'btn-primary', 'justify-content-center', 'align-items-center');\n  modalBtn.setAttribute('type', 'button');\n  modalBtn.setAttribute('data-toggle', 'modal');\n  modalBtn.setAttribute('data-target', '#modalWindow');\n  modalBtnImg.src = '../src/app/img/mainModalBtn.png';\n  modalText.classList.add('display-4', 'modalText');\n  modalText.innerHTML = 'Add Task';\n  navbar.append(myDay, tasks, importantTasks, plannedTasks, getModalWindowItem);\n  myDay.append(myDayLogo, myDayName);\n  tasks.append(tasksLogo, tasksName);\n  importantTasks.append(importantTasksLogo, importantTasksName);\n  getModalWindowItem.append(modalBtn, modalText);\n  modalBtn.append(modalBtnImg);\n  return navbar;\n}\n\n//# sourceURL=webpack:///./src/app/app-content/navbar/navbar.js?");

/***/ }),

/***/ "./src/app/app-content/navbar/planned-tasks.js":
/*!*****************************************************!*\
  !*** ./src/app/app-content/navbar/planned-tasks.js ***!
  \*****************************************************/
/*! exports provided: PlannedTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PlannedTasks\", function() { return PlannedTasks; });\nfunction PlannedTasks() {\n  var plannedTasks = document.createElement('div');\n  var plannedTasksBtn = document.createElement('button');\n  var plannedTasksLogo = document.createElement('img');\n  var plannedTasksName = document.createElement('a');\n  var dropdownMenu = document.createElement('div');\n  var onAWeek = document.createElement('a');\n  var onAMonth = document.createElement('a');\n  var onAYear = document.createElement('a');\n  plannedTasks.id = 'plannedTasks';\n  plannedTasks.classList.add('plannedTasks', 'btn-group', 'dropright');\n  plannedTasksBtn.id = 'plannedTasksBtn';\n  plannedTasksBtn.classList.add('btn', 'd-flex', 'row', 'm-0', 'justify-content-start', 'align-items-center', 'plannedTasksBtn', 'dropdown-toggle');\n  plannedTasksBtn.setAttribute('data-toggle', 'dropdown');\n  plannedTasksBtn.setAttribute('aria-haspopup', 'true');\n  plannedTasksBtn.setAttribute('aria-expanded', 'false');\n  plannedTasksBtn.setAttribute('type', 'button');\n  plannedTasksName.id = 'plannedTasksName';\n  plannedTasksName.classList.add('pl-3', 'nav-link');\n  plannedTasksName.innerHTML = 'Planned';\n  plannedTasksLogo.id = 'plannedTasksLogo';\n  plannedTasksLogo.src = '../src/app/img/plannedTasksLogo.png';\n  plannedTasksLogo.style.width = '2rem';\n  plannedTasksLogo.style.marginLeft = '2.5rem';\n  dropdownMenu.classList.add('dropdown-menu', 'dropdownMenu');\n  onAWeek.id = 'onAWeek';\n  onAWeek.classList.add('dropdown-item', 'onAWeek');\n  onAWeek.setAttribute('href', '#');\n  onAWeek.textContent = 'on a Week';\n  onAMonth.id = 'onAMonth';\n  onAMonth.classList.add('dropdown-item', 'onAMonth');\n  onAMonth.setAttribute('href', '#');\n  onAMonth.textContent = 'on a Month';\n  onAYear.id = 'onAYear';\n  onAYear.classList.add('dropdown-item', 'onAYear');\n  onAYear.setAttribute('href', '#');\n  onAYear.textContent = 'on a Year';\n  plannedTasks.append(plannedTasksBtn, dropdownMenu);\n  plannedTasksBtn.append(plannedTasksLogo, plannedTasksName);\n  dropdownMenu.append(onAWeek, onAMonth, onAYear);\n  return plannedTasks;\n}\n\n//# sourceURL=webpack:///./src/app/app-content/navbar/planned-tasks.js?");

/***/ }),

/***/ "./src/app/app.css":
/*!*************************!*\
  !*** ./src/app/app.css ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!./app.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/app.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/app.css?");

/***/ }),

/***/ "./src/app/app.js":
/*!************************!*\
  !*** ./src/app/app.js ***!
  \************************/
/*! exports provided: App */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"App\", function() { return App; });\n/* harmony import */ var _app_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.css */ \"./src/app/app.css\");\n/* harmony import */ var _app_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_app_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header */ \"./src/app/header/index.js\");\n/* harmony import */ var _app_content__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-content */ \"./src/app/app-content/index.js\");\n\n\n\nfunction App() {\n  var app = document.createElement('div');\n  var header = Object(_header__WEBPACK_IMPORTED_MODULE_1__[\"Header\"])();\n  var appContent = Object(_app_content__WEBPACK_IMPORTED_MODULE_2__[\"AppContent\"])();\n  app.classList.add('row-md-auto', 'app');\n  app.append(header, appContent);\n  return app;\n}\n\n//# sourceURL=webpack:///./src/app/app.js?");

/***/ }),

/***/ "./src/app/event-controls/index.js":
/*!*****************************************!*\
  !*** ./src/app/event-controls/index.js ***!
  \*****************************************/
/*! exports provided: eventControls */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"eventControls\", function() { return eventControls; });\n/* harmony import */ var _main_date_events__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main-date-events */ \"./src/app/event-controls/main-date-events/index.js\");\n/* harmony import */ var _main_content_events__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./main-content-events */ \"./src/app/event-controls/main-content-events/index.js\");\n\n\nfunction eventControls() {\n  Object(_main_date_events__WEBPACK_IMPORTED_MODULE_0__[\"MainDateEvents\"])();\n  Object(_main_content_events__WEBPACK_IMPORTED_MODULE_1__[\"MainContentEvents\"])();\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/add-modal-window/form-btn-group.js":
/*!***************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/add-modal-window/form-btn-group.js ***!
  \***************************************************************************************/
/*! exports provided: getFormBtnGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getFormBtnGroup\", function() { return getFormBtnGroup; });\nfunction getFormBtnGroup() {\n  var formBtnGroup = document.createElement('div');\n  var formBtnClose = document.createElement('button');\n  var formBtnAdd = document.createElement('button');\n  formBtnGroup.id = 'formBtnGroup';\n  formBtnGroup.classList.add('formBtnGroup');\n  formBtnGroup.setAttribute('role', 'group');\n  formBtnClose.id = 'formBtnClose';\n  formBtnClose.classList.add('btn', 'btn-default', 'formBtnClose');\n  formBtnClose.setAttribute('type', 'button');\n  formBtnClose.setAttribute('data-dismiss', 'modal');\n  formBtnClose.innerHTML = 'Close';\n  formBtnAdd.id = 'formBtnAdd';\n  formBtnAdd.classList.add('btn', 'btn-primary', 'formBtnAdd', 'ui-button');\n  formBtnAdd.setAttribute('type', 'submit');\n  formBtnAdd.innerHTML = 'Add Task';\n  formBtnGroup.append(formBtnClose, formBtnAdd);\n  return formBtnGroup;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/add-modal-window/form-btn-group.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/add-modal-window/index.js":
/*!******************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/add-modal-window/index.js ***!
  \******************************************************************************/
/*! exports provided: getMainModalWindow */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_window__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-window */ \"./src/app/event-controls/main-content-events/add-modal-window/modal-window.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"getMainModalWindow\", function() { return _modal_window__WEBPACK_IMPORTED_MODULE_0__[\"getMainModalWindow\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/add-modal-window/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/add-modal-window/modal-body.js":
/*!***********************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/add-modal-window/modal-body.js ***!
  \***********************************************************************************/
/*! exports provided: getMainModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getMainModalBody\", function() { return getMainModalBody; });\n/* harmony import */ var _form_btn_group__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form-btn-group */ \"./src/app/event-controls/main-content-events/add-modal-window/form-btn-group.js\");\n\nfunction getMainModalBody() {\n  var modalBody = document.createElement('div');\n  var modalBodyContainer = document.createElement('div');\n  var modalBodyForm = document.createElement('form');\n  var formTaskName = document.createElement('div');\n  var formTaskNameLabel = document.createElement('label');\n  var formTaskNameInput = document.createElement('input');\n  var formDateDeadline = document.createElement('div');\n  var formDateDeadlineLabel = document.createElement('label');\n  var formDateDeadlineInput = document.createElement('input');\n  var formImportantGroup = document.createElement('div');\n  var formImportantLabel = document.createElement('label');\n  var formImportantSelect = document.createElement('select');\n  var selectOptionFirst = document.createElement('option');\n  var selectOptionSecond = document.createElement('option');\n  var formBtnGroup = Object(_form_btn_group__WEBPACK_IMPORTED_MODULE_0__[\"getFormBtnGroup\"])();\n  modalBody.id = 'modalBody';\n  modalBody.classList.add('modal-body');\n  modalBodyContainer.id = 'modalBodyContainer';\n  modalBodyContainer.classList.add('container-fluid');\n  modalBodyForm.id = 'modalBodyForm';\n  modalBodyForm.classList.add('modalBodyForm');\n  formTaskName.id = 'formTaskName';\n  formTaskName.classList.add('form-group');\n  formTaskNameLabel.id = 'formTaskNameLabel';\n  formTaskNameLabel.classList.add('col-form-label');\n  formTaskNameLabel.innerHTML = 'Task Name:';\n  formTaskNameInput.id = 'formTaskNameInput';\n  formTaskNameInput.classList.add('form-control');\n  formTaskNameInput.setAttribute('type', 'text');\n  formTaskNameInput.setAttribute('name', 'taskName');\n  formDateDeadline.id = 'formDateDedline';\n  formDateDeadline.classList.add('form-group');\n  formDateDeadlineLabel.id = 'formDateDedlineLabel';\n  formDateDeadlineLabel.classList.add('col-form-label');\n  formDateDeadlineLabel.innerHTML = 'Date deadline:';\n  formDateDeadlineInput.id = 'formDateDeadlineInput';\n  formDateDeadlineInput.classList.add('form-control', 'datepicker');\n  formDateDeadlineInput.setAttribute('name', 'deadline');\n  formImportantGroup.id = 'formImportantGroup';\n  formImportantGroup.classList.add('form-group', 'btn-group-toggle');\n  formImportantGroup.setAttribute('data-toggle', 'buttons');\n  formImportantLabel.id = 'formImportantBtn';\n  formImportantLabel.classList.add('col-form-label');\n  formImportantLabel.innerHTML = 'Importance:';\n  formImportantSelect.id = 'formImportantSelect';\n  formImportantSelect.classList.add('form-control', 'formImportantSelect', 'custom-select');\n  formImportantSelect.setAttribute('name', 'important');\n  selectOptionFirst.id = 'selectOptionFirst';\n  selectOptionFirst.classList.add('selectOptionFirst');\n  selectOptionFirst.setAttribute('value', 'Minor');\n  selectOptionFirst.innerHTML = 'minor';\n  selectOptionSecond.id = 'selectOptionSecond';\n  selectOptionSecond.classList.add('selectOptionSecond');\n  selectOptionSecond.setAttribute('value', 'Important');\n  selectOptionSecond.innerHTML = 'important';\n  modalBody.append(modalBodyContainer);\n  modalBodyContainer.append(modalBodyForm);\n  modalBodyForm.append(formTaskName, formDateDeadline, formImportantGroup, formBtnGroup);\n  formTaskName.append(formTaskNameLabel, formTaskNameInput);\n  formDateDeadline.append(formDateDeadlineLabel, formDateDeadlineInput);\n  formImportantGroup.append(formImportantLabel, formImportantSelect);\n  formImportantSelect.append(selectOptionFirst, selectOptionSecond);\n  return modalBody;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/add-modal-window/modal-body.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/add-modal-window/modal-window.css":
/*!**************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/add-modal-window/modal-window.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./modal-window.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/add-modal-window/modal-window.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/add-modal-window/modal-window.css?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/add-modal-window/modal-window.js":
/*!*************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/add-modal-window/modal-window.js ***!
  \*************************************************************************************/
/*! exports provided: getMainModalWindow */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getMainModalWindow\", function() { return getMainModalWindow; });\n/* harmony import */ var _modal_window_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-window.css */ \"./src/app/event-controls/main-content-events/add-modal-window/modal-window.css\");\n/* harmony import */ var _modal_window_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_window_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-body */ \"./src/app/event-controls/main-content-events/add-modal-window/modal-body.js\");\n\n\nfunction getMainModalWindow() {\n  var modalWindow = document.createElement('div');\n  var modalDialog = document.createElement('div');\n  var modalContent = document.createElement('div');\n  var modalHeader = document.createElement('div');\n  var modalHeaderBtn = document.createElement('button');\n  var modalHeaderBtnSpan = document.createElement('span');\n  var modalHeaderText = document.createElement('h4');\n  var modalBody = Object(_modal_body__WEBPACK_IMPORTED_MODULE_1__[\"getMainModalBody\"])();\n  modalWindow.id = 'modalWindow';\n  modalWindow.classList.add('modal', 'fade', 'modalWindow');\n  modalWindow.setAttribute('tabindex', '-1');\n  modalWindow.setAttribute('role', 'dialog');\n  modalWindow.setAttribute('aria-labelledby', 'gridModalLabel');\n  modalWindow.setAttribute('aria-hidden', 'true');\n  modalDialog.id = 'modalDialog';\n  modalDialog.classList.add('modal-dialog');\n  modalDialog.setAttribute('role', 'document');\n  modalContent.id = 'modalContent';\n  modalContent.classList.add('modal-content');\n  modalHeader.id = 'modalHeader';\n  modalHeader.classList.add('modal-header');\n  modalHeader.style.background = 'url(../src/app/img/modalFormHeaderBg.png';\n  modalHeader.style.backgroundRepeat = 'no-repeat center center';\n  modalHeader.style.backgroundSize = '100% 100%';\n  modalHeaderBtn.id = 'modalHeaderBtn';\n  modalHeaderBtn.classList.add('close');\n  modalHeaderBtn.setAttribute('type', 'button');\n  modalHeaderBtn.setAttribute('data-dismiss', 'modal');\n  modalHeaderBtn.setAttribute('aria-label', 'Close');\n  modalHeaderBtnSpan.id = 'modalHeaderBtnSpan';\n  modalHeaderBtnSpan.setAttribute('aria-hidden', 'true');\n  modalHeaderBtnSpan.innerHTML = '&times;';\n  modalHeaderText.id = 'modalHeaderText';\n  modalHeaderText.classList.add('modal-title', 'display-4');\n  modalHeaderText.innerHTML = 'New Task';\n  modalWindow.append(modalDialog);\n  modalDialog.append(modalContent);\n  modalContent.append(modalHeader, modalBody);\n  modalHeader.append(modalHeaderText, modalHeaderBtn);\n  modalHeaderBtn.append(modalHeaderBtnSpan);\n  return modalWindow;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/add-modal-window/modal-window.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/date-picker.js":
/*!*******************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/date-picker.js ***!
  \*******************************************************************/
/*! exports provided: addDatePicker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addDatePicker\", function() { return addDatePicker; });\nfunction addDatePicker() {\n  $('.datepicker').datepicker({\n    dateFormat: \"yy-MM-dd\",\n    minDate: 0\n  });\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/date-picker.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/edit-modal-window/edit-modal-window.js":
/*!*******************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/edit-modal-window/edit-modal-window.js ***!
  \*******************************************************************************************/
/*! exports provided: getEditModalWindow */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getEditModalWindow\", function() { return getEditModalWindow; });\n/* harmony import */ var _modal_window_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-window.css */ \"./src/app/event-controls/main-content-events/edit-modal-window/modal-window.css\");\n/* harmony import */ var _modal_window_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_window_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-body */ \"./src/app/event-controls/main-content-events/edit-modal-window/modal-body.js\");\n\n\nfunction getEditModalWindow() {\n  var editModalWindow = document.createElement('div');\n  var modalDialog = document.createElement('div');\n  var modalContent = document.createElement('div');\n  var modalHeader = document.createElement('div');\n  var modalHeaderBtn = document.createElement('button');\n  var modalHeaderBtnSpan = document.createElement('span');\n  var modalHeaderText = document.createElement('h4');\n  var editModalBody = Object(_modal_body__WEBPACK_IMPORTED_MODULE_1__[\"getEditModalBody\"])();\n  editModalWindow.id = 'editModalWindow';\n  editModalWindow.classList.add('modal', 'fade', 'editModalWindow');\n  editModalWindow.setAttribute('tabindex', '-1');\n  editModalWindow.setAttribute('role', 'dialog');\n  editModalWindow.setAttribute('aria-labelledby', 'gridModalLabel');\n  editModalWindow.setAttribute('aria-hidden', 'true');\n  modalDialog.id = 'modalDialog';\n  modalDialog.classList.add('modal-dialog');\n  modalDialog.setAttribute('role', 'document');\n  modalContent.id = 'modalContent';\n  modalContent.classList.add('modal-content');\n  modalHeader.id = 'modalHeader';\n  modalHeader.classList.add('modal-header');\n  modalHeader.style.background = 'url(../src/app/img/editModalFormHeaderBg.png';\n  modalHeader.style.backgroundRepeat = 'no-repeat center center';\n  modalHeader.style.backgroundSize = '100% 100%';\n  modalHeaderBtn.id = 'modalHeaderBtn';\n  modalHeaderBtn.classList.add('close');\n  modalHeaderBtn.setAttribute('type', 'button');\n  modalHeaderBtn.setAttribute('data-dismiss', 'modal');\n  modalHeaderBtn.setAttribute('aria-label', 'Close');\n  modalHeaderBtnSpan.id = 'modalHeaderBtnSpan';\n  modalHeaderBtnSpan.setAttribute('aria-hidden', 'true');\n  modalHeaderBtnSpan.innerHTML = '&times;';\n  modalHeaderText.id = 'modalHeaderText';\n  modalHeaderText.classList.add('modal-title', 'display-4');\n  modalHeaderText.innerHTML = 'Edit Task';\n  editModalWindow.append(modalDialog);\n  modalDialog.append(modalContent);\n  modalContent.append(modalHeader, editModalBody);\n  modalHeader.append(modalHeaderText, modalHeaderBtn);\n  modalHeaderBtn.append(modalHeaderBtnSpan);\n  return editModalWindow;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/edit-modal-window/edit-modal-window.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/edit-modal-window/form-btn-group.js":
/*!****************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/edit-modal-window/form-btn-group.js ***!
  \****************************************************************************************/
/*! exports provided: getEditFormBtnGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getEditFormBtnGroup\", function() { return getEditFormBtnGroup; });\nfunction getEditFormBtnGroup() {\n  var editFormBtnGroup = document.createElement('div');\n  var editFormBtnClose = document.createElement('button');\n  var editFormBtnAdd = document.createElement('button');\n  editFormBtnGroup.id = 'editFormBtnGroup';\n  editFormBtnGroup.classList.add('editFormBtnGroup');\n  editFormBtnGroup.setAttribute('role', 'group');\n  editFormBtnClose.id = 'editFormBtnClose';\n  editFormBtnClose.classList.add('btn', 'btn-default', 'editFormBtnClose');\n  editFormBtnClose.setAttribute('type', 'button');\n  editFormBtnClose.setAttribute('data-dismiss', 'modal');\n  editFormBtnClose.innerHTML = 'Close';\n  editFormBtnAdd.id = 'editFormBtnAdd';\n  editFormBtnAdd.classList.add('btn', 'btn-primary', 'editFormBtnAdd', 'ui-button');\n  editFormBtnAdd.setAttribute('type', 'submit');\n  editFormBtnAdd.innerHTML = 'Edit Task';\n  editFormBtnGroup.append(editFormBtnClose, editFormBtnAdd);\n  return editFormBtnGroup;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/edit-modal-window/form-btn-group.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/edit-modal-window/index.js":
/*!*******************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/edit-modal-window/index.js ***!
  \*******************************************************************************/
/*! exports provided: getEditModalWindow */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _edit_modal_window__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit-modal-window */ \"./src/app/event-controls/main-content-events/edit-modal-window/edit-modal-window.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"getEditModalWindow\", function() { return _edit_modal_window__WEBPACK_IMPORTED_MODULE_0__[\"getEditModalWindow\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/edit-modal-window/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/edit-modal-window/modal-body.js":
/*!************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/edit-modal-window/modal-body.js ***!
  \************************************************************************************/
/*! exports provided: getEditModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getEditModalBody\", function() { return getEditModalBody; });\n/* harmony import */ var _form_btn_group__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form-btn-group */ \"./src/app/event-controls/main-content-events/edit-modal-window/form-btn-group.js\");\n\nfunction getEditModalBody() {\n  var editModalBody = document.createElement('div');\n  var editModalBodyContainer = document.createElement('div');\n  var editModalBodyForm = document.createElement('form');\n  var editFormTaskName = document.createElement('div');\n  var editFormTaskNameLabel = document.createElement('label');\n  var editFormTaskNameInput = document.createElement('input');\n  var editFormDateDeadline = document.createElement('div');\n  var editFormDateDeadlineLabel = document.createElement('label');\n  var editFormDateDeadlineInput = document.createElement('input');\n  var editFormImportantGroup = document.createElement('div');\n  var editFormImportantLabel = document.createElement('label');\n  var editFormImportantSelect = document.createElement('select');\n  var editSelectOptionFirst = document.createElement('option');\n  var editSelectOptionSecond = document.createElement('option');\n  var editFormBtnGroup = Object(_form_btn_group__WEBPACK_IMPORTED_MODULE_0__[\"getEditFormBtnGroup\"])();\n  editModalBody.id = 'editModalBody';\n  editModalBody.classList.add('modal-body');\n  editModalBodyContainer.id = 'editModalBodyContainer';\n  editModalBodyContainer.classList.add('container-fluid');\n  editModalBodyForm.id = 'editModalBodyForm';\n  editModalBodyForm.classList.add('editModalBodyForm');\n  editFormTaskName.id = 'editFormTaskName';\n  editFormTaskName.classList.add('form-group', 'editFormTaskName');\n  editFormTaskNameLabel.id = 'editFormTaskNameLabel';\n  editFormTaskNameLabel.classList.add('col-form-label');\n  editFormTaskNameLabel.innerHTML = 'Task Name:';\n  editFormTaskNameInput.id = 'editFormTaskNameInput';\n  editFormTaskNameInput.classList.add('form-control');\n  editFormTaskNameInput.setAttribute('type', 'text');\n  editFormTaskNameInput.setAttribute('name', 'taskName');\n  editFormTaskNameInput.classList.add('editFormTaskNameInput');\n  editFormDateDeadline.id = 'editFormDateDedline';\n  editFormDateDeadline.classList.add('form-group');\n  editFormDateDeadlineLabel.id = 'editFormDateDedlineLabel';\n  editFormDateDeadlineLabel.classList.add('col-form-label');\n  editFormDateDeadlineLabel.innerHTML = 'Date deadline:';\n  editFormDateDeadlineInput.id = 'feditFormDateDeadlineInput';\n  editFormDateDeadlineInput.classList.add('form-control', 'datepicker', 'editFormDateDeadlineInput');\n  editFormDateDeadlineInput.setAttribute('name', 'deadline');\n  editFormImportantGroup.id = 'editFormImportantGroup';\n  editFormImportantGroup.classList.add('form-group', 'btn-group-toggle');\n  editFormImportantGroup.setAttribute('data-toggle', 'buttons');\n  editFormImportantLabel.id = 'editFormImportantBtn';\n  editFormImportantLabel.classList.add('col-form-label');\n  editFormImportantLabel.innerHTML = 'Importance:';\n  editFormImportantSelect.id = 'editFormImportantSelect';\n  editFormImportantSelect.classList.add('form-control', 'editFormImportantSelect', 'custom-select');\n  editFormImportantSelect.setAttribute('name', 'important');\n  editSelectOptionFirst.id = 'editSelectOptionFirst';\n  editSelectOptionFirst.classList.add('editSelectOptionFirst');\n  editSelectOptionFirst.setAttribute('value', 'Minor');\n  editSelectOptionFirst.innerHTML = 'minor';\n  editSelectOptionSecond.id = 'editSelectOptionSecond';\n  editSelectOptionSecond.classList.add('editSelectOptionSecond');\n  editSelectOptionSecond.setAttribute('value', 'Important');\n  editSelectOptionSecond.innerHTML = 'important';\n  editModalBody.append(editModalBodyContainer);\n  editModalBodyContainer.append(editModalBodyForm);\n  editModalBodyForm.append(editFormTaskName, editFormDateDeadline, editFormImportantGroup, editFormBtnGroup);\n  editFormTaskName.append(editFormTaskNameLabel, editFormTaskNameInput);\n  editFormDateDeadline.append(editFormDateDeadlineLabel, editFormDateDeadlineInput);\n  editFormImportantGroup.append(editFormImportantLabel, editFormImportantSelect);\n  editFormImportantSelect.append(editSelectOptionFirst, editSelectOptionSecond);\n  return editModalBody;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/edit-modal-window/modal-body.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/edit-modal-window/modal-window.css":
/*!***************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/edit-modal-window/modal-window.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./modal-window.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/edit-modal-window/modal-window.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/edit-modal-window/modal-window.css?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/index.js":
/*!*************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/index.js ***!
  \*************************************************************/
/*! exports provided: MainContentEvents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MainContentEvents\", function() { return MainContentEvents; });\n/* harmony import */ var _add_modal_window__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-modal-window */ \"./src/app/event-controls/main-content-events/add-modal-window/index.js\");\n/* harmony import */ var _edit_modal_window__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit-modal-window */ \"./src/app/event-controls/main-content-events/edit-modal-window/index.js\");\n/* harmony import */ var _date_picker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./date-picker */ \"./src/app/event-controls/main-content-events/date-picker.js\");\n/* harmony import */ var _tasks_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tasks-service */ \"./src/app/event-controls/main-content-events/tasks-service/index.js\");\n/* harmony import */ var _navbar_sort_tasks__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./navbar-sort-tasks */ \"./src/app/event-controls/main-content-events/navbar-sort-tasks/index.js\");\n\n\n\n\n\nfunction MainContentEvents() {\n  var modalWindow = Object(_add_modal_window__WEBPACK_IMPORTED_MODULE_0__[\"getMainModalWindow\"])();\n  var editModalWindow = Object(_edit_modal_window__WEBPACK_IMPORTED_MODULE_1__[\"getEditModalWindow\"])();\n  document.body.append(modalWindow);\n  document.body.append(editModalWindow);\n  Object(_date_picker__WEBPACK_IMPORTED_MODULE_2__[\"addDatePicker\"])();\n  Object(_navbar_sort_tasks__WEBPACK_IMPORTED_MODULE_4__[\"EventsSortTasks\"])();\n  Object(_tasks_service__WEBPACK_IMPORTED_MODULE_3__[\"TasksService\"])();\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/navbar-sort-tasks/current-date.js":
/*!**************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/navbar-sort-tasks/current-date.js ***!
  \**************************************************************************************/
/*! exports provided: currentDate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"currentDate\", function() { return currentDate; });\nfunction currentDate() {\n  var currentDate = new Date();\n\n  Date.prototype.toShortFormat = function () {\n    var month_names = [\"Jan\", \"Feb\", \"Mar\", \"Apr\", \"May\", \"Jun\", \"Jul\", \"Aug\", \"Sep\", \"Oct\", \"Nov\", \"Dec\"];\n    var dd = '0' + currentDate.getDate();\n    var yyyy = currentDate.getFullYear();\n    var month_index = this.getMonth();\n    return yyyy + \"-\" + month_names[month_index] + \"-\" + dd;\n  };\n\n  var today = currentDate.toShortFormat();\n  return today;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/navbar-sort-tasks/current-date.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/navbar-sort-tasks/events-sort-tasks.js":
/*!*******************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/navbar-sort-tasks/events-sort-tasks.js ***!
  \*******************************************************************************************/
/*! exports provided: getMyDayTasks, getTasks, getImportantTasks, getOnAWeekTasks, getOnAMonth, getOnAYear, EventsSortTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getMyDayTasks\", function() { return getMyDayTasks; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getTasks\", function() { return getTasks; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getImportantTasks\", function() { return getImportantTasks; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getOnAWeekTasks\", function() { return getOnAWeekTasks; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getOnAMonth\", function() { return getOnAMonth; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getOnAYear\", function() { return getOnAYear; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"EventsSortTasks\", function() { return EventsSortTasks; });\n/* harmony import */ var _current_date__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./current-date */ \"./src/app/event-controls/main-content-events/navbar-sort-tasks/current-date.js\");\n\nfunction getMyDayTasks() {\n  var today = Object(_current_date__WEBPACK_IMPORTED_MODULE_0__[\"currentDate\"])();\n  var allNewTasks = document.getElementsByClassName('newTask');\n\n  for (var i = 0; i < allNewTasks.length; i++) {\n    var task = allNewTasks[i];\n\n    if (task.getAttribute('name') === today) {\n      task.style.display = 'block';\n      task.classList.add('d-flex');\n    } else {\n      task.style.display = 'none';\n      task.classList.remove('d-flex');\n    }\n  }\n}\nfunction getTasks() {\n  var allNewTasks = document.getElementsByClassName('newTask');\n\n  for (var i = 0; i < allNewTasks.length; i++) {\n    var task = allNewTasks[i];\n    task.style.display = 'block';\n    task.classList.add('d-flex');\n  }\n}\nfunction getImportantTasks() {\n  var allNewTasks = document.getElementsByClassName('newTask');\n\n  for (var i = 0; i < allNewTasks.length; i++) {\n    var task = allNewTasks[i];\n\n    if (task.getAttribute('data-important') === 'Important') {\n      task.style.display = 'block';\n      task.classList.add('d-flex');\n    } else {\n      task.style.display = 'none';\n      task.classList.remove('d-flex');\n    }\n  }\n}\nfunction getOnAWeekTasks() {\n  var allNewTasks = document.getElementsByClassName('newTask');\n  var weekMs = 604800000;\n  var today = new Date();\n  var todayMs = Date.parse(today);\n  var nextWeekMs = Number(todayMs) + Number(weekMs);\n\n  for (var i = 0; i < allNewTasks.length; i++) {\n    var task = allNewTasks[i];\n    var taskName = task.getAttribute('name');\n    var dedlineParse = Date.parse(taskName);\n\n    if (dedlineParse < nextWeekMs) {\n      task.style.display = 'block';\n      task.classList.add('d-flex');\n    } else {\n      task.style.display = 'none';\n      task.classList.remove('d-flex');\n    }\n  }\n}\nfunction getOnAMonth() {\n  var allNewTasks = document.getElementsByClassName('newTask');\n  var monthMs = 2592000000;\n  var today = new Date();\n  var todayMs = Date.parse(today);\n  var nextMonthMs = Number(todayMs) + Number(monthMs);\n\n  for (var i = 0; i < allNewTasks.length; i++) {\n    var task = allNewTasks[i];\n    var taskName = task.getAttribute('name');\n    var dedlineParse = Date.parse(taskName);\n\n    if (dedlineParse < nextMonthMs) {\n      task.style.display = 'block';\n      task.classList.add('d-flex');\n    } else {\n      task.style.display = 'none';\n      task.classList.remove('d-flex');\n    }\n  }\n}\nfunction getOnAYear() {\n  var allNewTasks = document.getElementsByClassName('newTask');\n  var yearMs = 31536000000;\n  var today = new Date();\n  var todayMs = Date.parse(today);\n  var nextYearMs = Number(todayMs) + Number(yearMs);\n\n  for (var i = 0; i < allNewTasks.length; i++) {\n    var task = allNewTasks[i];\n    var taskName = task.getAttribute('name');\n    var dedlineParse = Date.parse(taskName);\n\n    if (dedlineParse < nextYearMs) {\n      task.style.display = 'block';\n      task.classList.add('d-flex');\n    } else {\n      task.style.display = 'none';\n      task.classList.remove('d-flex');\n    }\n  }\n}\nfunction EventsSortTasks() {\n  document.getElementById(\"myDay\").addEventListener('click', getMyDayTasks);\n  document.getElementById(\"tasks\").addEventListener('click', getTasks);\n  document.getElementById(\"importantTasks\").addEventListener('click', getImportantTasks);\n  document.getElementById(\"onAWeek\").addEventListener('click', getOnAWeekTasks);\n  document.getElementById(\"onAMonth\").addEventListener('click', getOnAMonth);\n  document.getElementById(\"onAYear\").addEventListener('click', getOnAYear);\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/navbar-sort-tasks/events-sort-tasks.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/navbar-sort-tasks/index.js":
/*!*******************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/navbar-sort-tasks/index.js ***!
  \*******************************************************************************/
/*! exports provided: EventsSortTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _events_sort_tasks__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./events-sort-tasks */ \"./src/app/event-controls/main-content-events/navbar-sort-tasks/events-sort-tasks.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"EventsSortTasks\", function() { return _events_sort_tasks__WEBPACK_IMPORTED_MODULE_0__[\"EventsSortTasks\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/navbar-sort-tasks/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.css":
/*!****************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./add-task.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.css?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.js":
/*!***************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.js ***!
  \***************************************************************************************/
/*! exports provided: addTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addTask\", function() { return addTask; });\n/* harmony import */ var _add_task_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-task.css */ \"./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.css\");\n/* harmony import */ var _add_task_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_add_task_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _render_task__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./render-task */ \"./src/app/event-controls/main-content-events/tasks-service/add-task/render-task.js\");\n\n\nfunction addTask() {\n  document.getElementById('formBtnAdd').addEventListener('click', function (event) {\n    event.preventDefault();\n    var form = document.forms[\"modalBodyForm\"].elements;\n    var taskName = form.taskName.value;\n    var deadline = form.deadline.value;\n    var important = form.important.value;\n    var status = \"toDo\";\n    var data = {\n      taskName: taskName,\n      deadline: deadline,\n      important: important,\n      status: status\n    };\n    var requestOptions = {\n      method: 'POST',\n      body: JSON.stringify(data),\n      headers: {\n        'Content-Type': 'application/json'\n      }\n    };\n    fetch('http://localhost:3000/tasks', requestOptions).then(function (res) {\n      return res.json();\n    }).then(_render_task__WEBPACK_IMPORTED_MODULE_1__[\"getTaskTemplate\"]).then(_render_task__WEBPACK_IMPORTED_MODULE_1__[\"renderTask\"]).then(_render_task__WEBPACK_IMPORTED_MODULE_1__[\"bindTaskActions\"]).then(_render_task__WEBPACK_IMPORTED_MODULE_1__[\"deleteModal\"]);\n  });\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/add-task/index.js":
/*!************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/add-task/index.js ***!
  \************************************************************************************/
/*! exports provided: addTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _add_task__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-task */ \"./src/app/event-controls/main-content-events/tasks-service/add-task/add-task.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"addTask\", function() { return _add_task__WEBPACK_IMPORTED_MODULE_0__[\"addTask\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/add-task/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/add-task/render-task.js":
/*!******************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/add-task/render-task.js ***!
  \******************************************************************************************/
/*! exports provided: getTaskTemplate, renderTask, bindTaskActions, deleteModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getTaskTemplate\", function() { return getTaskTemplate; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"renderTask\", function() { return renderTask; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"bindTaskActions\", function() { return bindTaskActions; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deleteModal\", function() { return deleteModal; });\n/* harmony import */ var _edit_task__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../edit-task */ \"./src/app/event-controls/main-content-events/tasks-service/edit-task/index.js\");\n\nfunction getTaskTemplate(task) {\n  var newTask = document.createElement('div');\n  var toDoBtn = document.createElement('div');\n  var toDoBtnLabel = document.createElement('label');\n  var toDoBtnIput = document.createElement('input');\n  var taskInfo = document.createElement('div');\n  var taskTitle = document.createElement('div');\n  var taskOptions = document.createElement('div');\n  var importanceTaskBtn = document.createElement('div');\n  var taskDeadline = document.createElement('div');\n  var deadlineLogo = document.createElement('img');\n  var dateDeadine = document.createElement('div');\n  var editTaskTemplate = document.createElement('div');\n  var editTaskBtn = document.createElement('button');\n  var editTaskLogo = document.createElement('img');\n  var deleteTaskBtn = document.createElement('button');\n  var deleteTaskLogo = document.createElement('img');\n  newTask.setAttribute('id', task.id);\n  newTask.setAttribute('name', task.deadline);\n  newTask.setAttribute('data-important', task.important);\n  newTask.classList.add('newTask', 'd-flex', 'flex-row', 'align-items-center', 'justify-content-start', 'p-1', 'pl-4', 'm-0', 'border-bottom', 'border-left');\n  taskInfo.setAttribute('id', 'taskInfo');\n  taskInfo.classList.add('taskInfo', 'col', 'justify-content-start', 'align-items-center');\n  taskTitle.setAttribute('id', 'taskTitle');\n  taskTitle.classList.add('taskTitle', 'p-0');\n  taskTitle.textContent = JSON.stringify(task.taskName).replace(/[\"']/g, '');\n  taskOptions.setAttribute('id', 'taskOptions');\n  taskOptions.setAttribute('role', 'group');\n  taskOptions.classList.add('taskOptions', 'd-flex', 'flex-row', 'align-items-center', 'justify-content-start', 'p-0');\n  importanceTaskBtn.setAttribute('id', 'importanceTaskBtn');\n  importanceTaskBtn.classList.add('importanceTaskBtn', 'bg-light', 'p-0', 'align-items-center');\n  importanceTaskBtn.textContent = JSON.stringify(task.important).replace(/[\"']/g, '');\n  taskDeadline.setAttribute('id', 'taskDeadline');\n  taskDeadline.classList.add('taskDeadline', 'd-flex', 'flex-row', 'align-items-center');\n  deadlineLogo.setAttribute('id', 'deadlineLogo');\n  deadlineLogo.classList.add('deadlineLogo', 'm-2');\n  deadlineLogo.src = '../src/app/img/deadline.png';\n  deadlineLogo.style.width = '1.5rem';\n  dateDeadine.setAttribute('id', 'dateDeadine');\n  dateDeadine.classList.add('align-items-center', 'dateDeadine');\n  dateDeadine.textContent = JSON.stringify(task.deadline).replace(/[\"']/g, '');\n  toDoBtn.setAttribute('data-toggle', 'buttons');\n  toDoBtn.classList.add('toDoBtn', 'btn-group-toggle', 'd-flex', 'flex-row', 'align-items-center', 'justify-content-start');\n  toDoBtnLabel.classList.add('toDo-btn-label', 'btn', 'btn-outline-primary');\n  toDoBtnIput.id = 'toDoBtnIput';\n  toDoBtnIput.setAttribute('type', 'checkbox');\n  toDoBtnIput.setAttribute('autocomplete', 'off');\n  toDoBtnIput.classList.add('toDo-btn-input');\n  editTaskTemplate.id = 'editTaskTemplate';\n  editTaskTemplate.classList.add('updateTaskTemplate', 'btn-group', 'mr-4');\n  editTaskTemplate.setAttribute('role', 'group');\n  editTaskBtn.classList.add('edit-task-btn', 'btn', 'btn-warning');\n  editTaskBtn.setAttribute('type', 'button');\n  editTaskBtn.setAttribute('data-toggle', 'modal');\n  editTaskBtn.setAttribute('data-target', '#editModalWindow');\n  editTaskLogo.classList.add('editTaskLogo', 'm-2');\n  editTaskLogo.src = '../src/app/img/editTaskBtn.png';\n  editTaskLogo.style.width = '1.2rem';\n  deleteTaskBtn.id = 'deleteTaskBtn';\n  deleteTaskBtn.classList.add('deleteTaskBtn', 'btn', 'btn-danger');\n  deleteTaskLogo.classList.add('deleteTaskLogo', 'm-2');\n  deleteTaskLogo.src = '../src/app/img/deleteTaskBtn.png';\n  deleteTaskLogo.style.width = '1.2rem';\n  editTaskTemplate.append(editTaskBtn, deleteTaskBtn);\n  editTaskBtn.append(editTaskLogo);\n  deleteTaskBtn.append(deleteTaskLogo);\n  toDoBtn.append(toDoBtnLabel);\n  toDoBtnLabel.append(toDoBtnIput);\n  taskInfo.append(taskTitle, taskOptions);\n  taskOptions.append(importanceTaskBtn, taskDeadline);\n  taskDeadline.append(deadlineLogo, dateDeadine);\n  newTask.append(toDoBtn, taskInfo, editTaskTemplate);\n  return newTask;\n}\nfunction renderTask(taskTemplate) {\n  document.querySelector('.tasks').append(taskTemplate);\n  return taskTemplate.attributes.id.value;\n}\nfunction bindTaskActions(taskId) {\n  var taskRow = document.querySelector(\".newTask[id=\\\"\".concat(taskId, \"\\\"]\"));\n  var editTaskBtn = taskRow.querySelector('.edit-task-btn');\n  var toDoBtnInput = taskRow.querySelector('.toDo-btn-input');\n  editTaskBtn.addEventListener('click', function (e) {\n    document.querySelector('.editFormTaskNameInput').setAttribute('value', \"\".concat(taskRow.querySelector('.taskTitle').textContent));\n    document.querySelector('.editFormDateDeadlineInput').setAttribute('value', \"\".concat(taskRow.querySelector('.dateDeadine').textContent));\n    Object(_edit_task__WEBPACK_IMPORTED_MODULE_0__[\"editTask\"])(taskId);\n  });\n  toDoBtnInput.addEventListener('change', function (e) {\n    if (toDoBtnInput.checked) {\n      e.preventDefault();\n      var status = \"Do\";\n      var data = {\n        status: status\n      };\n      var requestOptions = {\n        method: 'PUT',\n        body: JSON.stringify(data),\n        headers: {\n          'Content-Type': 'application/json'\n        }\n      };\n      fetch(\"http://localhost:3000/tasks/\".concat(taskId), requestOptions).then(function (res) {\n        return res.json();\n      });\n    } else {\n      e.preventDefault();\n      var _status = \"toDo\";\n      var _data = {\n        status: _status\n      };\n      var _requestOptions = {\n        method: 'PUT',\n        body: JSON.stringify(_data),\n        headers: {\n          'Content-Type': 'application/json'\n        }\n      };\n      fetch(\"http://localhost:3000/tasks/\".concat(taskId), _requestOptions).then(function (res) {\n        return res.json();\n      });\n    }\n  });\n}\nfunction deleteModal(renderTask) {\n  $('#modalWindow').modal('hide');\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/add-task/render-task.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/all-tasks/get-all-tasks.js":
/*!*********************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/all-tasks/get-all-tasks.js ***!
  \*********************************************************************************************/
/*! exports provided: getAllTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getAllTasks\", function() { return getAllTasks; });\nfunction getAllTasks() {\n  var baseUrl = \"http://localhost:3000\";\n  return fetch(\"\".concat(baseUrl, \"/tasks/\")).then(function (r) {\n    return r.json();\n  });\n}\n;\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/all-tasks/get-all-tasks.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/all-tasks/index.js":
/*!*************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/all-tasks/index.js ***!
  \*************************************************************************************/
/*! exports provided: allTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"allTasks\", function() { return allTasks; });\n/* harmony import */ var _render_all_tasks__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render-all-tasks */ \"./src/app/event-controls/main-content-events/tasks-service/all-tasks/render-all-tasks.js\");\n\nfunction allTasks() {\n  Object(_render_all_tasks__WEBPACK_IMPORTED_MODULE_0__[\"getTasksTemplate\"])();\n  Object(_render_all_tasks__WEBPACK_IMPORTED_MODULE_0__[\"renderAllTasks\"])();\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/all-tasks/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/all-tasks/render-all-tasks.js":
/*!************************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/all-tasks/render-all-tasks.js ***!
  \************************************************************************************************/
/*! exports provided: getTasksTemplate, renderAllTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getTasksTemplate\", function() { return getTasksTemplate; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"renderAllTasks\", function() { return renderAllTasks; });\n/* harmony import */ var _add_task_render_task__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../add-task/render-task */ \"./src/app/event-controls/main-content-events/tasks-service/add-task/render-task.js\");\n/* harmony import */ var _get_all_tasks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./get-all-tasks */ \"./src/app/event-controls/main-content-events/tasks-service/all-tasks/get-all-tasks.js\");\n\n\nfunction getTasksTemplate() {\n  return Object(_get_all_tasks__WEBPACK_IMPORTED_MODULE_1__[\"getAllTasks\"])().then(function (data) {\n    return data.map(_add_task_render_task__WEBPACK_IMPORTED_MODULE_0__[\"getTaskTemplate\"]);\n  });\n}\nfunction renderAllTasks() {\n  return getTasksTemplate().then(function (data) {\n    return data.map(_add_task_render_task__WEBPACK_IMPORTED_MODULE_0__[\"renderTask\"]);\n  }).then(function (taskIds) {\n    taskIds.forEach(_add_task_render_task__WEBPACK_IMPORTED_MODULE_0__[\"bindTaskActions\"]);\n  });\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/all-tasks/render-all-tasks.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/delete-task/delete-task.js":
/*!*********************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/delete-task/delete-task.js ***!
  \*********************************************************************************************/
/*! exports provided: getDeleteTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getDeleteTask\", function() { return getDeleteTask; });\nfunction getDeleteTask() {\n  document.addEventListener(\"click\", function (e) {\n    if (e.target.id === 'deleteTaskBtn') {\n      var task = e.target.parentElement.parentElement;\n      var id = task.id;\n      var requestOptions = {\n        method: 'DELETE',\n        headers: {\n          'Content-Type': 'application/json'\n        }\n      };\n      fetch(\"http://localhost:3000/tasks/\".concat(id), requestOptions).then(task.remove());\n    }\n  });\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/delete-task/delete-task.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/delete-task/index.js":
/*!***************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/delete-task/index.js ***!
  \***************************************************************************************/
/*! exports provided: deleteTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deleteTask\", function() { return deleteTask; });\n/* harmony import */ var _delete_task__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./delete-task */ \"./src/app/event-controls/main-content-events/tasks-service/delete-task/delete-task.js\");\n\nfunction deleteTask() {\n  Object(_delete_task__WEBPACK_IMPORTED_MODULE_0__[\"getDeleteTask\"])();\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/delete-task/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/edit-task/edit-task.js":
/*!*****************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/edit-task/edit-task.js ***!
  \*****************************************************************************************/
/*! exports provided: editTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"editTask\", function() { return editTask; });\n/* harmony import */ var _add_task_render_task__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../add-task/render-task */ \"./src/app/event-controls/main-content-events/tasks-service/add-task/render-task.js\");\n\nfunction editTask(taskId) {\n  var putTask = document.getElementById('editFormBtnAdd').addEventListener('click', function (event) {\n    event.preventDefault();\n    var form = document.forms[\"editModalBodyForm\"].elements;\n    var taskName = form.taskName.value;\n    var deadline = form.deadline.value;\n    var important = form.important.value;\n    var status = \"toDo\";\n    var data = {\n      taskName: taskName,\n      deadline: deadline,\n      important: important,\n      status: status\n    };\n    var requestOptions = {\n      method: 'PUT',\n      body: JSON.stringify(data),\n      headers: {\n        'Content-Type': 'application/json'\n      }\n    };\n    fetch(\"http://localhost:3000/tasks/\".concat(taskId), requestOptions).then(function (res) {\n      return res.json();\n    }).then(function (data) {\n      var taskRow = document.querySelector(\".newTask[id=\\\"\".concat(taskId, \"\\\"]\"));\n      taskRow.querySelector('.dateDeadine').textContent = JSON.stringify(data.deadline).replace(/[\"']/g, '');\n      taskRow.querySelector('.taskTitle').textContent = JSON.stringify(data.taskName).replace(/[\"']/g, '');\n      taskRow.querySelector('.importanceTaskBtn').textContent = JSON.stringify(data.important).replace(/[\"']/g, '');\n    }).then($('#editModalWindow').modal('hide'));\n  });\n  return putTask;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/edit-task/edit-task.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/edit-task/index.js":
/*!*************************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/edit-task/index.js ***!
  \*************************************************************************************/
/*! exports provided: editTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _edit_task__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit-task */ \"./src/app/event-controls/main-content-events/tasks-service/edit-task/edit-task.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"editTask\", function() { return _edit_task__WEBPACK_IMPORTED_MODULE_0__[\"editTask\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/edit-task/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-content-events/tasks-service/index.js":
/*!***************************************************************************!*\
  !*** ./src/app/event-controls/main-content-events/tasks-service/index.js ***!
  \***************************************************************************/
/*! exports provided: TasksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TasksService\", function() { return TasksService; });\n/* harmony import */ var _add_task__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-task */ \"./src/app/event-controls/main-content-events/tasks-service/add-task/index.js\");\n/* harmony import */ var _all_tasks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./all-tasks */ \"./src/app/event-controls/main-content-events/tasks-service/all-tasks/index.js\");\n/* harmony import */ var _delete_task__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./delete-task */ \"./src/app/event-controls/main-content-events/tasks-service/delete-task/index.js\");\n\n\n\nfunction TasksService() {\n  Object(_add_task__WEBPACK_IMPORTED_MODULE_0__[\"addTask\"])();\n  Object(_all_tasks__WEBPACK_IMPORTED_MODULE_1__[\"allTasks\"])();\n  Object(_delete_task__WEBPACK_IMPORTED_MODULE_2__[\"deleteTask\"])();\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-content-events/tasks-service/index.js?");

/***/ }),

/***/ "./src/app/event-controls/main-date-events/background.js":
/*!***************************************************************!*\
  !*** ./src/app/event-controls/main-date-events/background.js ***!
  \***************************************************************/
/*! exports provided: getMyDayMainDateEvent, getTasksMainDateEvent, getImportantTasksMainDateEvent, getPlannedTasksMainDateEvent, addEventMainDate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getMyDayMainDateEvent\", function() { return getMyDayMainDateEvent; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getTasksMainDateEvent\", function() { return getTasksMainDateEvent; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getImportantTasksMainDateEvent\", function() { return getImportantTasksMainDateEvent; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getPlannedTasksMainDateEvent\", function() { return getPlannedTasksMainDateEvent; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addEventMainDate\", function() { return addEventMainDate; });\nfunction getMyDayMainDateEvent(event) {\n  var getActiveNavbarItem = document.getElementById('activeNavbarItem');\n  var getActiveNavbarItemText = getActiveNavbarItem.innerHTML = 'My Day';\n  return getActiveNavbarItemText, getMainDateDateEventBg();\n}\n\nfunction getMainDateDateEventBg() {\n  mainDate.style.background = 'url(../src/app/img/myDayBg.png';\n  mainDate.style.backgroundRepeat = 'no-repeat center center';\n  mainDate.style.backgroundSize = '100% 100%';\n}\n\nfunction getTasksMainDateEvent(event) {\n  var getActiveNavbarItem = document.getElementById('activeNavbarItem');\n  var getActiveNavbarItemText = getActiveNavbarItem.innerHTML = 'Tasks';\n  return getActiveNavbarItemText, getTasksMainDateEventBg();\n}\n\nfunction getTasksMainDateEventBg() {\n  mainDate.style.background = 'url(../src/app/img/tasksBg.jpg';\n  mainDate.style.backgroundRepeat = 'no-repeat center center';\n  mainDate.style.backgroundSize = '100% 100%';\n}\n\nfunction getImportantTasksMainDateEvent(event) {\n  var getActiveNavbarItem = document.getElementById('activeNavbarItem');\n  var getActiveNavbarItemText = getActiveNavbarItem.innerHTML = 'Important Tasks';\n  return getActiveNavbarItemText, getImportantTasksMainDateEventBg();\n}\n\nfunction getImportantTasksMainDateEventBg() {\n  mainDate.style.background = 'url(../src/app/img/importantTasksBg.png';\n  mainDate.style.backgroundRepeat = 'no-repeat center center';\n  mainDate.style.backgroundSize = '100% 100%';\n}\n\nfunction getPlannedTasksMainDateEvent(event) {\n  var getActiveNavbarItem = document.getElementById('activeNavbarItem');\n  var getActiveNavbarItemText = getActiveNavbarItem.innerHTML = 'Planned Tasks';\n  return getActiveNavbarItemText, getPlannedTasksMainDateEventBg();\n}\n\nfunction getPlannedTasksMainDateEventBg() {\n  mainDate.style.background = 'url(../src/app/img/plannedTasksBg.png';\n  mainDate.style.backgroundRepeat = 'no-repeat center center';\n  mainDate.style.backgroundSize = '100% 100%';\n}\n\nfunction addEventMainDate() {\n  document.getElementById(\"myDay\").addEventListener('click', getMyDayMainDateEvent);\n  document.getElementById(\"tasks\").addEventListener('click', getTasksMainDateEvent);\n  document.getElementById(\"importantTasks\").addEventListener('click', getImportantTasksMainDateEvent);\n  document.getElementById(\"plannedTasksBtn\").addEventListener('click', getPlannedTasksMainDateEvent);\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-date-events/background.js?");

/***/ }),

/***/ "./src/app/event-controls/main-date-events/current-date-text.js":
/*!**********************************************************************!*\
  !*** ./src/app/event-controls/main-date-events/current-date-text.js ***!
  \**********************************************************************/
/*! exports provided: addCurrentDateMainDateContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addCurrentDateMainDateContent\", function() { return addCurrentDateMainDateContent; });\nfunction addCurrentDateMainDateContent() {\n  var time = setInterval(function () {\n    var date = new Date();\n    var options = {\n      month: 'long',\n      day: 'numeric',\n      weekday: 'long',\n      timezone: 'UTC'\n    };\n    document.getElementById('currentDateItem').innerHTML = date.toLocaleString(\"en-US\", options);\n  });\n  return time;\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-date-events/current-date-text.js?");

/***/ }),

/***/ "./src/app/event-controls/main-date-events/index.js":
/*!**********************************************************!*\
  !*** ./src/app/event-controls/main-date-events/index.js ***!
  \**********************************************************/
/*! exports provided: MainDateEvents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MainDateEvents\", function() { return MainDateEvents; });\n/* harmony import */ var _current_date_text__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./current-date-text */ \"./src/app/event-controls/main-date-events/current-date-text.js\");\n/* harmony import */ var _background__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./background */ \"./src/app/event-controls/main-date-events/background.js\");\n\n\nfunction MainDateEvents() {\n  Object(_current_date_text__WEBPACK_IMPORTED_MODULE_0__[\"addCurrentDateMainDateContent\"])();\n  Object(_background__WEBPACK_IMPORTED_MODULE_1__[\"addEventMainDate\"])();\n}\n\n//# sourceURL=webpack:///./src/app/event-controls/main-date-events/index.js?");

/***/ }),

/***/ "./src/app/header/header.css":
/*!***********************************!*\
  !*** ./src/app/header/header.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./header.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/header.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./src/app/header/header.css?");

/***/ }),

/***/ "./src/app/header/header.js":
/*!**********************************!*\
  !*** ./src/app/header/header.js ***!
  \**********************************/
/*! exports provided: Header */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Header\", function() { return Header; });\n/* harmony import */ var _header_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.css */ \"./src/app/header/header.css\");\n/* harmony import */ var _header_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_header_css__WEBPACK_IMPORTED_MODULE_0__);\n\nfunction Header() {\n  var header = document.createElement('header');\n  var logo = document.createElement('div');\n  var brandImage = document.createElement('img');\n  var brandName = document.createElement('h3');\n  header.append(logo);\n  logo.append(brandImage);\n  logo.append(brandName);\n  header.classList.add('navbar', 'navbar-dark', 'row', 'm-0', 'header');\n  logo.classList.add('text-white', 'row', 'align-items-center', 'pl-4');\n  brandImage.src = '../src/app/img/logo.png';\n  brandImage.style.width = '2.5rem';\n  brandName.classList.add('ml-2');\n  brandName.innerHTML = 'TO DO LIST';\n  return header;\n}\n\n//# sourceURL=webpack:///./src/app/header/header.js?");

/***/ }),

/***/ "./src/app/header/index.js":
/*!*********************************!*\
  !*** ./src/app/header/index.js ***!
  \*********************************/
/*! exports provided: Header */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header */ \"./src/app/header/header.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Header\", function() { return _header__WEBPACK_IMPORTED_MODULE_0__[\"Header\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/index.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _app_app_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.css */ \"./src/app/app.css\");\n/* harmony import */ var _app_app_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_app_app_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _app_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app/app */ \"./src/app/app.js\");\n/* harmony import */ var _app_event_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/event-controls */ \"./src/app/event-controls/index.js\");\n\n\n\nvar app = Object(_app_app__WEBPACK_IMPORTED_MODULE_1__[\"App\"])();\ndocument.body.append(app);\nObject(_app_event_controls__WEBPACK_IMPORTED_MODULE_2__[\"eventControls\"])();\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });