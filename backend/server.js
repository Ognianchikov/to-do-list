const database = require("./db.json");
const json = require("body-parser").json;
const urlencoded = require("body-parser").urlencoded;
const cors = require("cors");
const PORT = 3000;
const urlencodedOptions = {
  extended: true
};
const express = require("express");
const app = express();

app.use(cors());
app.use(json());
app.use(urlencoded(urlencodedOptions));

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));

app.get("/", getSendAnswer);
app.get("/tasks", getTasks);
app.get("/tasks/:id", getTaskById);
app.post("/tasks", addNewTask);
app.put("/tasks/:id", updateTaskById);
app.delete("/tasks/:id", deleteTask);

function getSendAnswer(req, res) {
  res.send("ToDo just do it!");
}

function addNewTask(req, res) {
    const task = {
      deadline: req.body.deadline,
      taskName: req.body.taskName,
      important: req.body.important,
      status: req.body.status,
      id: Date.now(),
    };
    database.tasks.push(task);
    res.status(201).json(task);
}

function getTasks(req, res) {
  res.json(database.tasks);
}

function getTaskById(req, res) {
  res.json(database.tasks.find(task => task.id === +req.params.id));
}

function updateTaskById(req, res) {
  const task = database.tasks.find(task => task.id === +req.params.id);
  const taskUpdated = {
    ...task,
    ...req.body
  };
  console.log(req.params.id)
  const taskId = task.id;
  const tasksDatabase = [
    ...database.tasks.filter(task => task.id !== taskId),
    taskUpdated
  ];

  database.tasks = tasksDatabase;
  res.status(200).json(taskUpdated);
}

function deleteTask(req, res) {
  const task = database.tasks.find(task => task.id === +req.params.id);
  const taskId = task.id;

  const tasksDatabase = [...database.tasks.filter(task => task.id !== taskId)];
  database.tasks = tasksDatabase;

  console.log(database.tasks);
  res.sendStatus(200);
}


