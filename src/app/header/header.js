import './header.css'

export function Header() {

    const header = document.createElement('header');

    const logo = document.createElement('div');
    const brandImage = document.createElement('img');
    const brandName = document.createElement('h3');

    header.append(logo);

    logo.append(brandImage);
    logo.append(brandName);

    header.classList.add('navbar', 'navbar-dark', 'row', 'm-0', 'header');

    logo.classList.add('text-white', 'row', 'align-items-center', 'pl-4');
    brandImage.src = '../src/app/img/logo.png';
    brandImage.style.width = '2.5rem';
    brandName.classList.add('ml-2');
    brandName.innerHTML = 'TO DO LIST';

    return header;

}