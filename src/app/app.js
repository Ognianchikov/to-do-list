import './app.css'

import {Header} from './header';
import {AppContent} from './app-content';

export function App() {

    const app = document.createElement('div');
    const header = Header();
    const appContent  = AppContent();

    app.classList.add('row-md-auto', 'app');

    app.append(header, appContent);

    return app;
}
