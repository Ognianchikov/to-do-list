export function getEditFormBtnGroup() {

    const editFormBtnGroup = document.createElement('div');
    const editFormBtnClose = document.createElement('button');
    const editFormBtnAdd = document.createElement('button');

    editFormBtnGroup.id = 'editFormBtnGroup';
    editFormBtnGroup.classList.add('editFormBtnGroup');
    editFormBtnGroup.setAttribute('role', 'group');

    editFormBtnClose.id = 'editFormBtnClose';
    editFormBtnClose.classList.add('btn', 'btn-default', 'editFormBtnClose');
    editFormBtnClose.setAttribute('type', 'button');
    editFormBtnClose.setAttribute('data-dismiss', 'modal');
    editFormBtnClose.innerHTML = 'Close';

    editFormBtnAdd.id = 'editFormBtnAdd';
    editFormBtnAdd.classList.add('btn', 'btn-primary', 'editFormBtnAdd', 'ui-button');
    editFormBtnAdd.setAttribute('type', 'submit');
    editFormBtnAdd.innerHTML = 'Edit Task';


    editFormBtnGroup.append(editFormBtnClose, editFormBtnAdd);

    return editFormBtnGroup;

}