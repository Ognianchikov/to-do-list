import {getEditFormBtnGroup} from './form-btn-group';

export function getEditModalBody() { 

    const editModalBody = document.createElement('div');
    const editModalBodyContainer = document.createElement('div');

    const editModalBodyForm = document.createElement('form');

    const editFormTaskName = document.createElement('div');
    const editFormTaskNameLabel = document.createElement('label');
    const editFormTaskNameInput = document.createElement('input');

    const editFormDateDeadline = document.createElement('div');
    const editFormDateDeadlineLabel = document.createElement('label');
    const editFormDateDeadlineInput = document.createElement('input');

    const editFormImportantGroup = document.createElement('div');
    const editFormImportantLabel = document.createElement('label');
    const editFormImportantSelect = document.createElement('select');
    const editSelectOptionFirst = document.createElement('option');
    const editSelectOptionSecond = document.createElement('option');

    const editFormBtnGroup = getEditFormBtnGroup();

    editModalBody.id = 'editModalBody';
    editModalBody.classList.add('modal-body');

    editModalBodyContainer.id = 'editModalBodyContainer';
    editModalBodyContainer.classList.add('container-fluid');

    editModalBodyForm.id = 'editModalBodyForm';
    editModalBodyForm.classList.add('editModalBodyForm');

    editFormTaskName.id = 'editFormTaskName';
    editFormTaskName.classList.add('form-group', 'editFormTaskName');

    editFormTaskNameLabel.id = 'editFormTaskNameLabel';
    editFormTaskNameLabel.classList.add('col-form-label');
    editFormTaskNameLabel.innerHTML = 'Task Name:';

    editFormTaskNameInput.id = 'editFormTaskNameInput';
    editFormTaskNameInput.classList.add('form-control');
    editFormTaskNameInput.setAttribute('type', 'text');
    editFormTaskNameInput.setAttribute('name', 'taskName');
    editFormTaskNameInput.classList.add('editFormTaskNameInput');

    editFormDateDeadline.id = 'editFormDateDedline';
    editFormDateDeadline.classList.add('form-group');

    editFormDateDeadlineLabel.id = 'editFormDateDedlineLabel';
    editFormDateDeadlineLabel.classList.add('col-form-label');
    editFormDateDeadlineLabel.innerHTML = 'Date deadline:';

    editFormDateDeadlineInput.id = 'feditFormDateDeadlineInput';
    editFormDateDeadlineInput.classList.add('form-control', 'datepicker', 'editFormDateDeadlineInput');
    editFormDateDeadlineInput.setAttribute('name', 'deadline');

    editFormImportantGroup.id = 'editFormImportantGroup';
    editFormImportantGroup.classList.add('form-group', 'btn-group-toggle');
    editFormImportantGroup.setAttribute('data-toggle', 'buttons')

    editFormImportantLabel.id = 'editFormImportantBtn';
    editFormImportantLabel.classList.add('col-form-label');
    editFormImportantLabel.innerHTML = 'Importance:';

    editFormImportantSelect.id = 'editFormImportantSelect';
    editFormImportantSelect.classList.add('form-control', 'editFormImportantSelect', 'custom-select');
    editFormImportantSelect.setAttribute('name', 'important');

    editSelectOptionFirst.id = 'editSelectOptionFirst';
    editSelectOptionFirst.classList.add('editSelectOptionFirst');
    editSelectOptionFirst.setAttribute('value', 'Minor');
    editSelectOptionFirst.innerHTML = 'minor';

    editSelectOptionSecond.id = 'editSelectOptionSecond';
    editSelectOptionSecond.classList.add('editSelectOptionSecond');
    editSelectOptionSecond.setAttribute('value', 'Important');
    editSelectOptionSecond.innerHTML = 'important';

    editModalBody.append(editModalBodyContainer);
    editModalBodyContainer.append(editModalBodyForm);

    editModalBodyForm.append(editFormTaskName, editFormDateDeadline, editFormImportantGroup, editFormBtnGroup);

    editFormTaskName.append(editFormTaskNameLabel, editFormTaskNameInput);
    editFormDateDeadline.append(editFormDateDeadlineLabel, editFormDateDeadlineInput);
    editFormImportantGroup.append(editFormImportantLabel, editFormImportantSelect);
    
    editFormImportantSelect.append(editSelectOptionFirst, editSelectOptionSecond);

    return editModalBody;
}