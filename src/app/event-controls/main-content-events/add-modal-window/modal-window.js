import './modal-window.css';

import {getMainModalBody} from './modal-body';

export function getMainModalWindow() {

    const modalWindow = document.createElement('div');
    const modalDialog = document.createElement('div');
    const modalContent = document.createElement('div');

    const modalHeader = document.createElement('div');
    const modalHeaderBtn = document.createElement('button');
    const modalHeaderBtnSpan = document.createElement('span');
    const modalHeaderText = document.createElement('h4');
    
    const modalBody = getMainModalBody();

    modalWindow.id = 'modalWindow';
    modalWindow.classList.add('modal', 'fade', 'modalWindow');
    modalWindow.setAttribute('tabindex', '-1');
    modalWindow.setAttribute('role', 'dialog');
    modalWindow.setAttribute('aria-labelledby', 'gridModalLabel');
    modalWindow.setAttribute('aria-hidden', 'true');

    modalDialog.id = 'modalDialog';
    modalDialog.classList.add('modal-dialog');
    modalDialog.setAttribute('role', 'document');

    modalContent.id = 'modalContent';
    modalContent.classList.add('modal-content');

    modalHeader.id = 'modalHeader';
    modalHeader.classList.add('modal-header');
    modalHeader.style.background = 'url(../src/app/img/modalFormHeaderBg.png';
    modalHeader.style.backgroundRepeat = 'no-repeat center center';
    modalHeader.style.backgroundSize = '100% 100%'

    modalHeaderBtn.id = 'modalHeaderBtn';
    modalHeaderBtn.classList.add('close');
    modalHeaderBtn.setAttribute('type', 'button');
    modalHeaderBtn.setAttribute('data-dismiss', 'modal');
    modalHeaderBtn.setAttribute('aria-label', 'Close');

    modalHeaderBtnSpan.id = 'modalHeaderBtnSpan';
    modalHeaderBtnSpan.setAttribute('aria-hidden', 'true');
    modalHeaderBtnSpan.innerHTML = '&times;';

    modalHeaderText.id = 'modalHeaderText';
    modalHeaderText.classList.add('modal-title', 'display-4');
    modalHeaderText.innerHTML = 'New Task';

    modalWindow.append(modalDialog);
    modalDialog.append(modalContent);
    modalContent.append(modalHeader, modalBody);

    modalHeader.append(modalHeaderText, modalHeaderBtn);
    modalHeaderBtn.append(modalHeaderBtnSpan);

    return modalWindow;
}
