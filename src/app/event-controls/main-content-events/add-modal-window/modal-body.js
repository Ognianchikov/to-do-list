import {getFormBtnGroup} from './form-btn-group';

export function getMainModalBody() { 

    const modalBody = document.createElement('div');
    const modalBodyContainer = document.createElement('div');

    const modalBodyForm = document.createElement('form');

    const formTaskName = document.createElement('div');
    const formTaskNameLabel = document.createElement('label');
    const formTaskNameInput = document.createElement('input');

    const formDateDeadline = document.createElement('div');
    const formDateDeadlineLabel = document.createElement('label');
    const formDateDeadlineInput = document.createElement('input');

    const formImportantGroup = document.createElement('div');
    const formImportantLabel = document.createElement('label');
    const formImportantSelect = document.createElement('select');
    const selectOptionFirst = document.createElement('option');
    const selectOptionSecond = document.createElement('option');

    const formBtnGroup = getFormBtnGroup();

    modalBody.id = 'modalBody';
    modalBody.classList.add('modal-body');

    modalBodyContainer.id = 'modalBodyContainer';
    modalBodyContainer.classList.add('container-fluid');

    modalBodyForm.id = 'modalBodyForm';
    modalBodyForm.classList.add('modalBodyForm');

    formTaskName.id = 'formTaskName';
    formTaskName.classList.add('form-group');

    formTaskNameLabel.id = 'formTaskNameLabel';
    formTaskNameLabel.classList.add('col-form-label');
    formTaskNameLabel.innerHTML = 'Task Name:';

    formTaskNameInput.id = 'formTaskNameInput';
    formTaskNameInput.classList.add('form-control');
    formTaskNameInput.setAttribute('type', 'text');
    formTaskNameInput.setAttribute('name', 'taskName');

    formDateDeadline.id = 'formDateDedline';
    formDateDeadline.classList.add('form-group');

    formDateDeadlineLabel.id = 'formDateDedlineLabel';
    formDateDeadlineLabel.classList.add('col-form-label');
    formDateDeadlineLabel.innerHTML = 'Date deadline:';

    formDateDeadlineInput.id = 'formDateDeadlineInput';
    formDateDeadlineInput.classList.add('form-control', 'datepicker');
    formDateDeadlineInput.setAttribute('name', 'deadline');

    formImportantGroup.id = 'formImportantGroup';
    formImportantGroup.classList.add('form-group', 'btn-group-toggle');
    formImportantGroup.setAttribute('data-toggle', 'buttons')

    formImportantLabel.id = 'formImportantBtn';
    formImportantLabel.classList.add('col-form-label');
    formImportantLabel.innerHTML = 'Importance:';

    formImportantSelect.id = 'formImportantSelect';
    formImportantSelect.classList.add('form-control', 'formImportantSelect', 'custom-select');
    formImportantSelect.setAttribute('name', 'important');

    selectOptionFirst.id = 'selectOptionFirst';
    selectOptionFirst.classList.add('selectOptionFirst');
    selectOptionFirst.setAttribute('value', 'Minor');
    selectOptionFirst.innerHTML = 'minor';

    selectOptionSecond.id = 'selectOptionSecond';
    selectOptionSecond.classList.add('selectOptionSecond');
    selectOptionSecond.setAttribute('value', 'Important');
    selectOptionSecond.innerHTML = 'important';

    modalBody.append(modalBodyContainer);
    modalBodyContainer.append(modalBodyForm);

    modalBodyForm.append(formTaskName, formDateDeadline, formImportantGroup, formBtnGroup);

    formTaskName.append(formTaskNameLabel, formTaskNameInput);
    formDateDeadline.append(formDateDeadlineLabel, formDateDeadlineInput);
    formImportantGroup.append(formImportantLabel, formImportantSelect);
    
    formImportantSelect.append(selectOptionFirst, selectOptionSecond);

    return modalBody;
}