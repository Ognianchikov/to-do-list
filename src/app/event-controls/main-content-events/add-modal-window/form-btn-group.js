export function getFormBtnGroup() {

    const formBtnGroup = document.createElement('div');
    const formBtnClose = document.createElement('button');
    const formBtnAdd = document.createElement('button');

    formBtnGroup.id = 'formBtnGroup';
    formBtnGroup.classList.add('formBtnGroup');
    formBtnGroup.setAttribute('role', 'group');

    formBtnClose.id = 'formBtnClose';
    formBtnClose.classList.add('btn', 'btn-default', 'formBtnClose');
    formBtnClose.setAttribute('type', 'button');
    formBtnClose.setAttribute('data-dismiss', 'modal');
    formBtnClose.innerHTML = 'Close';

    formBtnAdd.id = 'formBtnAdd';
    formBtnAdd.classList.add('btn', 'btn-primary', 'formBtnAdd', 'ui-button');
    formBtnAdd.setAttribute('type', 'submit');
    formBtnAdd.innerHTML = 'Add Task';


    formBtnGroup.append(formBtnClose, formBtnAdd);

    return formBtnGroup;

}