import {getMainModalWindow} from './add-modal-window';
import {getEditModalWindow} from './edit-modal-window';
import {addDatePicker} from './date-picker';
import {TasksService} from './tasks-service';
import {EventsSortTasks} from './navbar-sort-tasks';

export function MainContentEvents() {
    
    const modalWindow = getMainModalWindow();
    const editModalWindow = getEditModalWindow();

    document.body.append(modalWindow);
    document.body.append(editModalWindow);
    
    addDatePicker();
    EventsSortTasks();
    TasksService();
}