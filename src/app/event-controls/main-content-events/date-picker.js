export function addDatePicker() {   
    
    $('.datepicker').datepicker({
        dateFormat: "yy-MM-dd",
        minDate: 0
    })

}