import {currentDate} from './current-date';

export function getMyDayTasks() {
    const today = currentDate();
    const allNewTasks = document.getElementsByClassName('newTask');
    
    for(let i = 0; i < allNewTasks.length; i++) {
        const task = allNewTasks[i];
        if(task.getAttribute('name') === today){
            task.style.display = 'block';
            task.classList.add('d-flex');
        } else {
            task.style.display = 'none';
            task.classList.remove('d-flex');
        }
    }
}

export function getTasks() {
    const allNewTasks = document.getElementsByClassName('newTask');

    for(let i = 0; i < allNewTasks.length; i++) {
        const task = allNewTasks[i];
        task.style.display = 'block';
        task.classList.add('d-flex');
    }
}

export function getImportantTasks() {
    const allNewTasks = document.getElementsByClassName('newTask');

    for(let i = 0; i < allNewTasks.length; i++) {
        const task = allNewTasks[i];
        if(task.getAttribute('data-important') === 'Important'){
            task.style.display = 'block';
            task.classList.add('d-flex');
        } else {
            task.style.display = 'none';
            task.classList.remove('d-flex');
        }
    }

}

export function getOnAWeekTasks(){
    const allNewTasks = document.getElementsByClassName('newTask');
    const weekMs = 604800000;
    const today = new Date();
    const todayMs = Date.parse(today);
    const nextWeekMs = Number(todayMs) + Number(weekMs);

    for(let i = 0; i < allNewTasks.length; i++) {
        const task = allNewTasks[i];
        const taskName = task.getAttribute('name');
        const dedlineParse = Date.parse(taskName);

        if(dedlineParse < nextWeekMs){
            task.style.display = 'block';
            task.classList.add('d-flex');
        } else {
            task.style.display = 'none';
            task.classList.remove('d-flex');
        }
    }

}

export function getOnAMonth() {
    const allNewTasks = document.getElementsByClassName('newTask');
    const monthMs = 2592000000;
    const today = new Date();
    const todayMs = Date.parse(today);
    const nextMonthMs = Number(todayMs) + Number(monthMs);

    for(let i = 0; i < allNewTasks.length; i++) {
        const task = allNewTasks[i];
        const taskName = task.getAttribute('name');
        const dedlineParse = Date.parse(taskName);

        if(dedlineParse < nextMonthMs){
            task.style.display = 'block';
            task.classList.add('d-flex');
        } else {
            task.style.display = 'none';
            task.classList.remove('d-flex');
        }
    }
}

export function getOnAYear() {
    const allNewTasks = document.getElementsByClassName('newTask');
    const yearMs = 31536000000;
    const today = new Date();
    const todayMs = Date.parse(today);
    const nextYearMs = Number(todayMs) + Number(yearMs);

    for(let i = 0; i < allNewTasks.length; i++) {
        const task = allNewTasks[i];
        const taskName = task.getAttribute('name');
        const dedlineParse = Date.parse(taskName);

        if(dedlineParse < nextYearMs){
            task.style.display = 'block';
            task.classList.add('d-flex');
        } else {
            task.style.display = 'none';
            task.classList.remove('d-flex');
        }
    }
}

export function EventsSortTasks() {
    document.getElementById("myDay").addEventListener('click', getMyDayTasks);
    document.getElementById("tasks").addEventListener('click', getTasks);
    document.getElementById("importantTasks").addEventListener('click', getImportantTasks);
    document.getElementById("onAWeek").addEventListener('click', getOnAWeekTasks);
    document.getElementById("onAMonth").addEventListener('click', getOnAMonth);
    document.getElementById("onAYear").addEventListener('click', getOnAYear);
}