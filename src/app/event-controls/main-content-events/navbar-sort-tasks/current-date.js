    
export function currentDate() {    
    let currentDate = new Date(); 

    Date.prototype.toShortFormat = function() {

    let month_names =["Jan","Feb","Mar",
                      "Apr","May","Jun",
                      "Jul","Aug","Sep",
                      "Oct","Nov","Dec"];

    let dd = '0' + currentDate.getDate();
    let yyyy = currentDate.getFullYear();
    let month_index = this.getMonth();

    return yyyy + "-" + month_names[month_index] + "-" + dd;
    } 
    let today = currentDate.toShortFormat()

    return today;
}