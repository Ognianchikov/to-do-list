import {addTask} from './add-task'
import {allTasks} from './all-tasks';
import {deleteTask} from './delete-task';

export function TasksService() {
    addTask();
    allTasks();
    deleteTask();
}