
export function getAllTasks() {        
     const baseUrl = `http://localhost:3000`;
            
    return fetch(`${baseUrl}/tasks/`)
        .then(r => r.json())
};
