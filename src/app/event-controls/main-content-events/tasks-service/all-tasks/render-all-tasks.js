import {getTaskTemplate, renderTask, bindTaskActions} from '../add-task/render-task';
import {getAllTasks} from './get-all-tasks';

export function getTasksTemplate() {  
    return getAllTasks().then(data => data.map(getTaskTemplate));
}

export function renderAllTasks(){
    return getTasksTemplate().then(data => data.map(renderTask)).then(taskIds => {
        taskIds.forEach(bindTaskActions);
    });
}