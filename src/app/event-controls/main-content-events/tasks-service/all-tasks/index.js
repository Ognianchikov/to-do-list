import {getTasksTemplate, renderAllTasks} from './render-all-tasks';

export function allTasks() {
    getTasksTemplate();
    renderAllTasks();
}