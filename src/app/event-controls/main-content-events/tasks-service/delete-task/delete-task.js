
export function getDeleteTask() {
    
    document.addEventListener("click", function(e) {
        if(e.target.id === 'deleteTaskBtn') {

            const task = e.target.parentElement.parentElement;
            const id = task.id;

            const requestOptions = {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
            }       
        
            fetch(`http://localhost:3000/tasks/${id}`, requestOptions)
                .then(task.remove())
            }
        }
    );
      
}