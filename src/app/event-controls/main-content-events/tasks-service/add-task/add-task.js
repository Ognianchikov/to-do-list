import './add-task.css'
import {getTaskTemplate, renderTask, bindTaskActions, deleteModal} from './render-task';


export function addTask() {
    
document.getElementById('formBtnAdd').addEventListener('click', function(event) {
    event.preventDefault();
    
    const form = document.forms["modalBodyForm"].elements;
    const taskName = form.taskName.value;
    const deadline = form.deadline.value;
    const important = form.important.value;
    const status = "toDo";

    const data = {
        taskName,
        deadline,
        important,
        status
    };
    
    const requestOptions = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        },
    }


    fetch('http://localhost:3000/tasks', requestOptions)
        .then( res => res.json())
        .then(getTaskTemplate)
        .then(renderTask)
        .then(bindTaskActions)
        .then(deleteModal)
    });

}
