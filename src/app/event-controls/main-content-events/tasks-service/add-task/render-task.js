import {editTask} from '../edit-task'

export function getTaskTemplate(task) {

    const newTask = document.createElement('div');

    const toDoBtn = document.createElement('div');
    const toDoBtnLabel = document.createElement('label');
    const toDoBtnIput = document.createElement('input');

    const taskInfo = document.createElement('div');
    const taskTitle = document.createElement('div');

    const taskOptions = document.createElement('div');
    const importanceTaskBtn = document.createElement('div');

    const taskDeadline = document.createElement('div');
    const deadlineLogo = document.createElement('img');
    const dateDeadine = document.createElement('div');

    const editTaskTemplate = document.createElement('div');
    const editTaskBtn = document.createElement('button');
    const editTaskLogo = document.createElement('img');
    const deleteTaskBtn = document.createElement('button');
    const deleteTaskLogo = document.createElement('img');

    newTask.setAttribute('id', task.id);
    newTask.setAttribute('name', task.deadline);
    newTask.setAttribute('data-important', task.important)
    newTask.classList.add('newTask', 'd-flex', 'flex-row', 'align-items-center', 'justify-content-start', 'p-1', 'pl-4', 'm-0', 'border-bottom', 'border-left');

    taskInfo.setAttribute('id', 'taskInfo');
    taskInfo.classList.add('taskInfo', 'col', 'justify-content-start', 'align-items-center');

    taskTitle.setAttribute('id', 'taskTitle');
    taskTitle.classList.add('taskTitle', 'p-0');
    taskTitle.textContent = JSON.stringify(task.taskName).replace(/["']/g,'');

    taskOptions.setAttribute('id', 'taskOptions');
    taskOptions.setAttribute('role', 'group');
    taskOptions.classList.add('taskOptions', 'd-flex', 'flex-row', 'align-items-center', 'justify-content-start','p-0');

    importanceTaskBtn.setAttribute('id', 'importanceTaskBtn');
    importanceTaskBtn.classList.add('importanceTaskBtn', 'bg-light', 'p-0', 'align-items-center');
    importanceTaskBtn.textContent = JSON.stringify(task.important).replace(/["']/g,'');

    taskDeadline.setAttribute('id', 'taskDeadline');
    taskDeadline.classList.add('taskDeadline', 'd-flex', 'flex-row','align-items-center')

    deadlineLogo.setAttribute('id', 'deadlineLogo');
    deadlineLogo.classList.add('deadlineLogo', 'm-2');
    deadlineLogo.src = '../src/app/img/deadline.png';
    deadlineLogo.style.width = '1.5rem';

    dateDeadine.setAttribute('id', 'dateDeadine');
    dateDeadine.classList.add('align-items-center', 'dateDeadine');
    dateDeadine.textContent = JSON.stringify(task.deadline).replace(/["']/g,'');

    toDoBtn.setAttribute('data-toggle', 'buttons');
    toDoBtn.classList.add('toDoBtn', 'btn-group-toggle', 'd-flex', 'flex-row', 'align-items-center', 'justify-content-start');

    toDoBtnLabel.classList.add('toDo-btn-label', 'btn', 'btn-outline-primary');           

    toDoBtnIput.id = 'toDoBtnIput';
    toDoBtnIput.setAttribute('type', 'checkbox');
    toDoBtnIput.setAttribute('autocomplete', 'off');
    toDoBtnIput.classList.add('toDo-btn-input');

    editTaskTemplate.id = 'editTaskTemplate';
    editTaskTemplate.classList.add('updateTaskTemplate', 'btn-group', 'mr-4');
    editTaskTemplate.setAttribute('role', 'group');

    editTaskBtn.classList.add('edit-task-btn', 'btn', 'btn-warning');  
    editTaskBtn.setAttribute('type', 'button');
    editTaskBtn.setAttribute('data-toggle', 'modal');
    editTaskBtn.setAttribute('data-target', '#editModalWindow');

    editTaskLogo.classList.add('editTaskLogo', 'm-2');
    editTaskLogo.src = '../src/app/img/editTaskBtn.png';
    editTaskLogo.style.width = '1.2rem';

    deleteTaskBtn.id = 'deleteTaskBtn';
    deleteTaskBtn.classList.add('deleteTaskBtn', 'btn', 'btn-danger');

    deleteTaskLogo.classList.add('deleteTaskLogo', 'm-2');
    deleteTaskLogo.src = '../src/app/img/deleteTaskBtn.png';
    deleteTaskLogo.style.width = '1.2rem';

    editTaskTemplate.append(editTaskBtn, deleteTaskBtn)
    editTaskBtn.append(editTaskLogo);
    deleteTaskBtn.append(deleteTaskLogo);

    toDoBtn.append(toDoBtnLabel);
    toDoBtnLabel.append(toDoBtnIput);

    taskInfo.append(taskTitle, taskOptions);

    taskOptions.append(importanceTaskBtn, taskDeadline);
    taskDeadline.append(deadlineLogo, dateDeadine)

    newTask.append(toDoBtn, taskInfo, editTaskTemplate);

    return newTask;
} 

export function renderTask(taskTemplate) {
    document.querySelector('.tasks').append(taskTemplate);
    return taskTemplate.attributes.id.value;
}


export function bindTaskActions(taskId) {
    const taskRow = document.querySelector(`.newTask[id="${taskId}"]`);
    const editTaskBtn = taskRow.querySelector('.edit-task-btn');
    const toDoBtnInput = taskRow.querySelector('.toDo-btn-input');

    editTaskBtn.addEventListener('click', e => {
        document.querySelector('.editFormTaskNameInput').setAttribute('value', `${taskRow.querySelector('.taskTitle').textContent}`);
        document.querySelector('.editFormDateDeadlineInput').setAttribute('value', `${taskRow.querySelector('.dateDeadine').textContent}`);
        editTask(taskId);
    })

    toDoBtnInput.addEventListener('change', e => {

        if(toDoBtnInput.checked){
            e.preventDefault();
            const status = "Do";
            const data = {
                status
            };
            
            const requestOptions = {
                method: 'PUT',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                },
            }    
        
            fetch(`http://localhost:3000/tasks/${taskId}`, requestOptions)
                .then( res => res.json())
        } else{
            e.preventDefault();
            const status = "toDo";
            const data = {
                status
            };
            
            const requestOptions = {
                method: 'PUT',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                },
            }   
        
            fetch(`http://localhost:3000/tasks/${taskId}`, requestOptions)
                .then( res => res.json())
        }
    })
}

export function deleteModal(renderTask) {
    $('#modalWindow').modal('hide');
}