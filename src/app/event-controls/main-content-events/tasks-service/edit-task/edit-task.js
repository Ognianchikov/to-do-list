import {getTaskTemplate, renderTask} from '../add-task/render-task'

export function editTask(taskId) {
    const putTask = document.getElementById('editFormBtnAdd').addEventListener('click', function(event) {
        event.preventDefault();

        const form = document.forms["editModalBodyForm"].elements;
        const taskName = form.taskName.value;
        const deadline = form.deadline.value;
        const important = form.important.value;
        const status = "toDo";
    
        const data = {
            taskName,
            deadline,
            important,
            status
        };
        
        const requestOptions = {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }
    
        fetch(`http://localhost:3000/tasks/${taskId}`, requestOptions)
            .then( res => res.json())
            .then(data => {
                const taskRow = document.querySelector(`.newTask[id="${taskId}"]`);
                taskRow.querySelector('.dateDeadine').textContent = JSON.stringify(data.deadline).replace(/["']/g,'');
                taskRow.querySelector('.taskTitle').textContent = JSON.stringify(data.taskName).replace(/["']/g,'');
                taskRow.querySelector('.importanceTaskBtn').textContent = JSON.stringify(data.important).replace(/["']/g,'');           
            })
            .then($('#editModalWindow').modal('hide'))
        });

    return putTask;
}