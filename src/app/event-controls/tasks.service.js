
    class TasksService {
        baseUrl = `http://localhost:3000`;

        getAllTasks() {
            return fetch(`${this.baseUrl}/tasks/`).then(r => r.json());
        };

        getTaskById = (id) => {
            return fetch(`${this.baseUrl}/tasks/${id}`).then(r => r.json());
        }
    }

    const tasksService = new TasksService();

    tasksService.getAllTasks().then(data => console.log(data));
    tasksService.getTaskById(2).then(data => console.log(data));;

    console.log(tasksService);

