
export function addCurrentDateMainDateContent(){
    const time = setInterval(function() {
        const date = new Date();
        const options = {
            month: 'long',
            day: 'numeric',
            weekday: 'long',
            timezone: 'UTC',
          };
        document.getElementById('currentDateItem').innerHTML = (date.toLocaleString("en-US", options));
    });

    return time;
}