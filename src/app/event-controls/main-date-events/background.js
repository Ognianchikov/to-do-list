export function getMyDayMainDateEvent(event) {
    const getActiveNavbarItem = document.getElementById('activeNavbarItem');
    const getActiveNavbarItemText = getActiveNavbarItem.innerHTML = 'My Day'

    return getActiveNavbarItemText, getMainDateDateEventBg() ;
}

function getMainDateDateEventBg() {
    mainDate.style.background = 'url(../src/app/img/myDayBg.png';
    mainDate.style.backgroundRepeat = 'no-repeat center center';
    mainDate.style.backgroundSize = '100% 100%';
}

export function getTasksMainDateEvent(event) {
    const getActiveNavbarItem = document.getElementById('activeNavbarItem');
    const getActiveNavbarItemText = getActiveNavbarItem.innerHTML = 'Tasks';

    return getActiveNavbarItemText, getTasksMainDateEventBg();
}

function getTasksMainDateEventBg() {
    mainDate.style.background = 'url(../src/app/img/tasksBg.jpg';
    mainDate.style.backgroundRepeat = 'no-repeat center center';
    mainDate.style.backgroundSize = '100% 100%';
}

export function getImportantTasksMainDateEvent(event) {
    const getActiveNavbarItem = document.getElementById('activeNavbarItem');
    const getActiveNavbarItemText = getActiveNavbarItem.innerHTML = 'Important Tasks';
    
    return getActiveNavbarItemText, getImportantTasksMainDateEventBg();
}

function getImportantTasksMainDateEventBg() {
    mainDate.style.background = 'url(../src/app/img/importantTasksBg.png';
    mainDate.style.backgroundRepeat = 'no-repeat center center';
    mainDate.style.backgroundSize = '100% 100%';
}

export function getPlannedTasksMainDateEvent(event) {
    const getActiveNavbarItem = document.getElementById('activeNavbarItem');
    const getActiveNavbarItemText = getActiveNavbarItem.innerHTML = 'Planned Tasks';
    
    return getActiveNavbarItemText, getPlannedTasksMainDateEventBg();
}

function getPlannedTasksMainDateEventBg() {
    mainDate.style.background = 'url(../src/app/img/plannedTasksBg.png'
    mainDate.style.backgroundRepeat = 'no-repeat center center';
    mainDate.style.backgroundSize = '100% 100%';
}

export function addEventMainDate() {
    document.getElementById("myDay").addEventListener('click', getMyDayMainDateEvent);
    document.getElementById("tasks").addEventListener('click', getTasksMainDateEvent);
    document.getElementById("importantTasks").addEventListener('click', getImportantTasksMainDateEvent);
    document.getElementById("plannedTasksBtn").addEventListener('click', getPlannedTasksMainDateEvent);
}