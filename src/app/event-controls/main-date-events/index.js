import {addCurrentDateMainDateContent} from './current-date-text';
import {addEventMainDate} from './background';

export function MainDateEvents() {
    addCurrentDateMainDateContent();
    addEventMainDate();
}