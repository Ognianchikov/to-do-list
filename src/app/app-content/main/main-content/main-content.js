import './main-content.css';

export function MainContent() {    
    
    const mainContent = document.createElement('div');

    mainContent.id = 'mainContent';
    mainContent.classList.add('flex-column', 'mainContent', 'tasks');

    return mainContent;
}