import './main-date.css';

export function MainDate() {
    
    const mainDate = document.createElement('div');
    const mainDateContent = document.createElement('div');
    const activeNavbarItem = document.createElement('div');
    const currentDateItem = document.createElement('div');

    mainDate.append(mainDateContent);
    mainDateContent.append(activeNavbarItem);
    mainDateContent.append(currentDateItem);

    activeNavbarItem.classList.add('display-3');

    mainDate.id = 'mainDate';
    mainDateContent.id = 'mainDateContent';
    activeNavbarItem.id = 'activeNavbarItem';
    currentDateItem.id = 'currentDateItem';

    mainDate.classList.add('mainDate')
    mainDate.style.background = 'url(../src/app/img/tasksBg.jpg';
    mainDate.style.backgroundRepeat = 'no-repeat center center';
    mainDate.style.backgroundSize = '100% 100%';

    mainDateContent.classList.add('flex-column', 'mainDateContent');

    activeNavbarItem.innerHTML = 'Tasks';

    return mainDate;
}