import './main.css'

import {MainDate} from './main-date';
import {MainContent} from './main-content';

export function Main() {

    const mainDate = MainDate();
    const mainContent = MainContent();

    const main = document.createElement('main');

    main.id = 'main';
    main.classList.add('col-9', 'main');

    main.append(mainDate, mainContent);

    return main;
}