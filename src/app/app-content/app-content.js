import './app-content.css'

import {NavBar} from './navbar';
import {Main} from './main';


export function AppContent() {

    const navbar = NavBar();
    const main = Main();
    
    const appContent = document.createElement('div');

    appContent.classList.add('row', 'align-items-start', 'app-content', 'display-3', 'appContent');
    appContent.append(navbar, main);

    return appContent;
}