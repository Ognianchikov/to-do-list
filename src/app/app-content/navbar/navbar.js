import './navbar.css'

import {PlannedTasks} from './planned-tasks'

export function NavBar() {

    const navbar = document.createElement('nav');

    const myDay = document.createElement('button');
    const myDayLogo = document.createElement('img');
    const myDayName = document.createElement('a');
    const tasks = document.createElement('button');
    const tasksLogo = document.createElement('img');
    const tasksName = document.createElement('a');
    const importantTasks = document.createElement('button');
    const importantTasksLogo = document.createElement('img');
    const importantTasksName = document.createElement('a');
    const plannedTasks = PlannedTasks();

    const getModalWindowItem = document.createElement('div');
    const modalBtn = document.createElement('button');
    const modalBtnImg = document.createElement('img');
    const modalText = document.createElement('div');

    navbar.id = 'navbar';
    navbar.classList.add('col-3', 'flex-column', 'p-0', 'navbar');

    myDay.id = 'myDay';
    myDayLogo.id = 'myDayLogo';
    myDayName.id = 'myDayName';
    
    myDay.classList.add('btn', 'd-flex', 'row', 'justify-content-start', 'align-items-center');

    myDayLogo.src = '../src/app/img/myDayLogo.png';
    myDayLogo.style.width = '2rem';
    myDayLogo.style.marginLeft = '2.5rem';
    myDayName.innerHTML = 'My day';
    myDayName.classList.add('pl-3', 'nav-link')

    tasks.id = 'tasks';
    tasksLogo.id = 'tasksLogo';
    tasksName.id = 'tasksName';

    tasks.classList.add('btn', 'd-flex', 'row', 'justify-content-start', 'align-items-center');
    tasksName.classList.add('pl-3', 'nav-link');

    tasksLogo.src = '../src/app/img/tasksLogo.png';
    tasksLogo.style.width = '2rem';
    tasksLogo.style.marginLeft = '2.5rem';
    tasksName.innerHTML = 'Tasks';

    importantTasks.id = 'importantTasks';
    importantTasksLogo.id = 'importantTasksLogo';
    importantTasksName.id = 'importantTasksName';

    importantTasks.classList.add('btn', 'd-flex', 'row', 'justify-content-start', 'align-items-center', 'importantTasks');
    importantTasksName.classList.add('pl-3', 'nav-link');

    importantTasksLogo.src = '../src/app/img/importantTasksLogo.png';
    importantTasksLogo.style.width = '2rem';
    importantTasksLogo.style.marginLeft = '2.5rem';
    importantTasksName.innerHTML = 'Important';

    getModalWindowItem.id = 'getModalWindowItem';
    modalBtn.id = 'modalBtn';
    modalBtnImg.id = 'modalBtnImg';
    modalText.id = 'mainModalText';
    getModalWindowItem.classList.add('row', 'justify-content-start', 'align-items-center', 'getModalWindowItem');
    modalBtn.classList.add('btn', 'btn-primary', 'justify-content-center', 'align-items-center');
    modalBtn.setAttribute('type', 'button');
    modalBtn.setAttribute('data-toggle', 'modal');
    modalBtn.setAttribute('data-target', '#modalWindow');
    modalBtnImg.src = '../src/app/img/mainModalBtn.png';

    modalText.classList.add('display-4', 'modalText')
    modalText.innerHTML = 'Add Task';
    
    navbar.append(myDay, tasks, importantTasks, plannedTasks, getModalWindowItem);

    myDay.append(myDayLogo, myDayName);
    tasks.append(tasksLogo, tasksName)
    importantTasks.append(importantTasksLogo, importantTasksName);
    getModalWindowItem.append(modalBtn, modalText);
    modalBtn.append(modalBtnImg)

    return navbar;
}
