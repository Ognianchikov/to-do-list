export function PlannedTasks() {
    const plannedTasks = document.createElement('div');
    const plannedTasksBtn = document.createElement('button');
    const plannedTasksLogo = document.createElement('img');
    const plannedTasksName = document.createElement('a');

    const dropdownMenu = document.createElement('div');
    const onAWeek = document.createElement('a');
    const onAMonth = document.createElement('a');
    const onAYear = document.createElement('a');

    plannedTasks.id = 'plannedTasks';
    plannedTasks.classList.add('plannedTasks', 'btn-group', 'dropright');

    plannedTasksBtn.id = 'plannedTasksBtn';
    plannedTasksBtn.classList.add('btn', 'd-flex', 'row', 'm-0', 'justify-content-start', 'align-items-center', 'plannedTasksBtn', 'dropdown-toggle');
    plannedTasksBtn.setAttribute('data-toggle', 'dropdown');
    plannedTasksBtn.setAttribute('aria-haspopup', 'true');
    plannedTasksBtn.setAttribute('aria-expanded', 'false');
    plannedTasksBtn.setAttribute('type', 'button');

    plannedTasksName.id = 'plannedTasksName';
    plannedTasksName.classList.add('pl-3', 'nav-link');    
    plannedTasksName.innerHTML = 'Planned';

    plannedTasksLogo.id = 'plannedTasksLogo';
    plannedTasksLogo.src = '../src/app/img/plannedTasksLogo.png';
    plannedTasksLogo.style.width = '2rem';
    plannedTasksLogo.style.marginLeft = '2.5rem';

    dropdownMenu.classList.add('dropdown-menu', 'dropdownMenu');

    onAWeek.id = 'onAWeek';
    onAWeek.classList.add('dropdown-item', 'onAWeek');
    onAWeek.setAttribute('href', '#');
    onAWeek.textContent = 'on a Week';

    onAMonth.id = 'onAMonth';
    onAMonth.classList.add('dropdown-item', 'onAMonth');
    onAMonth.setAttribute('href', '#');
    onAMonth.textContent = 'on a Month';

    onAYear.id = 'onAYear';
    onAYear.classList.add('dropdown-item', 'onAYear');
    onAYear.setAttribute('href', '#');
    onAYear.textContent = 'on a Year';

    plannedTasks.append(plannedTasksBtn, dropdownMenu);
    plannedTasksBtn.append(plannedTasksLogo, plannedTasksName);
    dropdownMenu.append(onAWeek, onAMonth, onAYear);

    return plannedTasks;
}