import './app/app.css';

import {App} from './app/app';
import {eventControls} from './app/event-controls'
 
const app = App();
document.body.append(app);

eventControls();
